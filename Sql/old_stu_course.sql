-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2019 at 01:55 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_ms`
--

-- --------------------------------------------------------

--
-- Table structure for table `old_stu_course`
--

CREATE TABLE `old_stu_course` (
  `id` int(100) NOT NULL,
  `stu_id` varchar(100) NOT NULL DEFAULT '0',
  `course` varchar(100) NOT NULL DEFAULT '0',
  `sub_course` varchar(100) NOT NULL DEFAULT '0',
  `fees` varchar(100) NOT NULL DEFAULT '0',
  `c_status` varchar(30) NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `old_stu_course`
--

INSERT INTO `old_stu_course` (`id`, `stu_id`, `course`, `sub_course`, `fees`, `c_status`, `created_at`, `updated_at`) VALUES
(14, 'HCC-12-0001', 'CMA', 'CMA 2', '50000', 'active', '2019-11-06 18:42:55', '2019-11-06 18:42:55'),
(16, 'HCC-12-0001', 'BIT', 'BIT 2', '3000', 'active', '2019-11-08 00:51:51', '2019-11-08 00:51:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `old_stu_course`
--
ALTER TABLE `old_stu_course`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `old_stu_course`
--
ALTER TABLE `old_stu_course`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
