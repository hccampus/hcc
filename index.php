
<?php  
session_start();

if (isset($_SESSION["username"])){
    ?>
<script>
window.location="view/index/home.php";
</script>
<?php
  //  header("location: ");
} 
include './controle/db.php';
$query_campus = mysqli_query(connect(), "SELECT * from my_campus ");
    $row_campus = mysqli_fetch_array($query_campus);
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="model/student/Student_img/logo (3).png" type="image/x-icon" >
  <title>Institute | Management System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="src/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="src/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="src/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="src/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="src/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
      .box.box-solid.box-primary {
    border: 2px solid #3c8dbc;
   
}
      .login-page, .register-page {
    background: #99d2e4;
}
  </style>
  <style>
.vibrate {
  animation: shake 0.5s;
  animation-iteration-count: 1s;
}

@keyframes shake {
  0% { transform: translate(1px, 1px) rotate(0deg); }
  10% { transform: translate(-1px, -2px) rotate(-1deg); }
  20% { transform: translate(-3px, 0px) rotate(1deg); }
  30% { transform: translate(3px, 2px) rotate(0deg); }
  40% { transform: translate(1px, -1px) rotate(1deg); }
  50% { transform: translate(-1px, 2px) rotate(-1deg); }
  60% { transform: translate(-3px, 1px) rotate(0deg); }
  70% { transform: translate(3px, 1px) rotate(-1deg); }
  80% { transform: translate(-1px, -1px) rotate(1deg); }
  90% { transform: translate(1px, 2px) rotate(0deg); }
  100% { transform: translate(1px, -2px) rotate(-1deg); }
}
</style>
  <!-- Google Font -->
  
</head>
<body class="hold-transition login-page">
<div class="login-box" id="body">
  <div class="login-logo">
      <img  src="model/company/image/<?= $row_campus['image'] ?>"  alt="User Image" style="width: 100px;height: 100px">
  <br>
    <b><?= $row_campus['company_name'] ?></b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body box box-primary  box-solid">
      <div class="">
          <h4 id="alert"> Sign in to start your session</h4>
      </div>
    
<div class="box-body">
    <form  method="post" id="login_form">
      <div class="form-group has-feedback" >
          <input type="text" class="form-control" placeholder="User Name" name="username" id="user_name" required="">
        <span id="icon" class="glyphicon glyphicon-user alert-info form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input id="but" type="password" class="form-control disabled" placeholder="Password" name="password" required="">
          <span class="glyphicon glyphicon-lock form-control-feedback alert-info" id="icon1"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button  class="btn btn-primary btn-block btn-flat ">Sign In</button>
        
          
        </div> 
        <!-- /.col -->
      </div>
    </form>
            </div>
   
      <style>
          a:hover{
              background-color: blueviolet;
              font-size: 16px;
          }
      </style>
  
  </div>
  <!--div class="col-md-12 alert alert-warning"><a href="student_app.php">! Click To Aplay for courses.Please Fill up the form Carefully.</a></div -->
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="src/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="src/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

<script>
$(document).on('submit', '#login_form', function(event){
		event.preventDefault();
		var body = $('#body');
                var span = $('#icon1');
                span.removeClass('glyphicon glyphicon-ban-circle alert-danger alert-info');
                 body.removeClass('vibrate');
		var form_data = $(this).serialize();
		$.ajax({
			url:"model/login/index.php",
			method:"POST",
			data:form_data,
                         dataType: 'json',
			success:function(data)
			{
                          span.addClass(data.class1);
                           body.addClass(data.class);
				$("#alert").html(data.alert);
                                if(data.location){
                                    window.location = data.location;
                                }
			}
		})
	});
        
        $(document).on('change', '#user_name', function(){
		var username = $(this).val();
                var body = $('#body');
                var span = $('#icon');
                
                body.removeClass('vibrate');
                 
                span.removeClass('glyphicon glyphicon-ban-circle alert-danger alert-info');
                 span.removeClass('glyphicon glyphicon-ok-circle alert-success alert-info');
                
                
		$.ajax({
			url:"model/login/index.php",
			method:"POST",
			data:{action: username },
                        dataType: 'json',
			success:function(data)
			{
                          span.addClass(data.class1);
                           body.addClass(data.class);
				$("#alert").html(data.alert);
                                
                             
                                 
			}
                        
		})
                
	});

</script>
</body>
</html>