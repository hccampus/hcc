<?php

include "config.php";

function connect()
{
    $connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    return $connection;
}

function ol_create($connection, $request)
{
    $query = "INSERT INTO `ol` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `ol` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

function ol_getCount($connection, $data)
{

    $query = "SELECT * FROM `ol` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}




function al_create($connection, $request)
{
    $query = "INSERT INTO `al` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `al` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

function al_getCount($connection, $data)
{

    $query = "SELECT * FROM `al` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}



function emr_create($connection, $request)
{
    $query = "INSERT INTO `emergency` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `emergency` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

function emr_getCount2($connection, $data)
{

    $query = "SELECT * FROM `emergency` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}



function personal_create($connection, $request)
{

   $query = "INSERT INTO `user` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `user` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}


function  personal_getCount($connection, $data)
{

    $query = "SELECT * FROM `user` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}


 
function course_create($connection, $request)
{
    $query = "INSERT INTO `course` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `course` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
 function cou_edit($connection, $request,$id)
{
   
       
           mysqli_query($connection, "UPDATE `course` SET colour='$request' WHERE `course_code`='$id'");
        
        
        return false;
    }
    function full_cou_edit($connection, $request,$id)
{
   
        foreach ($request as $column => $value) {
           mysqli_query($connection, "UPDATE `course` SET `$column`='$value' WHERE `id`='$id'");
        }
        
        return false;
    }

function course_getCount($connection, $data)
{

    $query = "SELECT * FROM `course` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}
function payment_create($connection, $request)
{
    $query = "INSERT INTO `payments` () VALUES ()";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `payments` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
function payment_getCount($connection, $data)
{

    $query = "SELECT * FROM `payments` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}
function mlt_create($connection, $request)
{
    $query = "INSERT INTO `mlt_course` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `mlt_course` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

function image($connection, $request)
{
   $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "Student_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
    $query = "INSERT INTO `user` (`created_at`,`image`) VALUES (NOW(),'$image')";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `user` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
}
    
 function scourse_create($connection, $request)
{
    $query = "INSERT INTO `stu_course` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `stu_course` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

 function old_course_create($connection, $request)
{
    $query = "INSERT INTO `old_stu_course` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `old_stu_course` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}

function old_course_getCount($connection, $data)
{

    $query = "SELECT * FROM `old_stu_course` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}

function Scourse_getCount($connection, $data)
{

    $query = "SELECT * FROM `stu_course` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}
 function user_edit($connection, $request,$id)
{
     
    
    if (isset($_POST["image"]["name"])){
        $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "Student_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
                                    
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `user` SET `$column`='$value',image='$image' WHERE `stu_id`='$id'");
        }
                                }
      
    
    }else {
       foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `user` SET `$column`='$value' WHERE `stu_id`='$id'");
        }
                                
        
       
 }
  return false;
        }
        
        function img_edit($connection, $request,$id)
{
     
    
    
        $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "../../model/student/Student_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
                                    
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `user` SET image='$image' WHERE `stu_id`='$id'");
        }
                                }
      
    
    
               return false;                  
        
       
 }
 
        

    function ol_edit($connection, $request,$id)
{
   
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `ol` SET `$column`='$value' WHERE `stu_id`='$id'");
        }
        
        return false;
    }
    
    function al_edit($connection, $request,$id)
{
   
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `al` SET `$column`='$value' WHERE `stu_id`='$id'");
        }
        
        return false;
    }
    
    function emr_edit($connection, $request,$id)
{
   
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `emergency` SET `$column`='$value' WHERE `stu_id`='$id'");
        }
        
        return false;
    }
    
    function course_edit($connection, $request,$id)
{
   
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `stu_course` SET `$column`='$value' WHERE `stu_id`='$id'");
        }
        
        return false;
    }
    
     function course_edit_p($connection, $request,$id,$course)
{
   
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `stu_course` SET `$column`='$value' WHERE `stu_id`='$id' && `course`='$course'");
        }
        
        return false;
    }
    
    
    function users_create_img($connection, $password,$request)
{
   $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "../../model/users/users_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
    $query = "INSERT INTO `users` (`created_at`,`image`,password) VALUES (NOW(),'$image','$password')";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `users` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
}

 function users_edit_img($connection, $request,$id)
{
     
    
    
        $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "../../model/users/users_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
                                    
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `users` SET image='$image',`$column`='$value' WHERE `id`='$id'");
        }
                                }
      
    
    
               return false;                  
        
       
 }
 
 function users_create($connection,$password ,$request)
{

   $query = "INSERT INTO `users` (`created_at`,password) VALUES (NOW(),'$password')";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `users` SET  password = '$password', `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
function users_edit($connection, $request,$id)
{
     
    
    
        
                                    
       foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `users` SET `$column`='$value' WHERE `id`='$id'");
        }
        
        return false;
                                }
      
    
    
              

function  users_getcount($connection, $data)
{

    $query = "SELECT * FROM `users` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}



    function lecturer_create_img($connection,$request)
{
   $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "../../model/lecturer/lecturer_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
    $query = "INSERT INTO `lecturer` (`created_at`,`image`) VALUES (NOW(),'$image')";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `lecturer` SET `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
}

 function lecturer_edit_img($connection, $request,$id)
{
     
    
    
        $image = $_FILES["image"]["name"];
                                $tmp_name = $_FILES["image"]["tmp_name"];
                                $path = "../../model/lecturer/lecturer_img/" . $image;
                                $image1 = explode(".", $image);
                                $ext = $image1[1];
                                $allowed = array("jpg", "jpeg", "png");
                               
                                if (in_array($ext, $allowed)) {
                                    move_uploaded_file($tmp_name, $path);
                                    
        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `lecturer` SET image='$image',`$column`='$value' WHERE `id`='$id'");
        }
                                }
      
    
    
               return false;                  
        
       
 }
 
 function lecturer_create($connection,$request)
{

   $query = "INSERT INTO `lecturer` (`created_at`) VALUES (NOW())";
    $results = mysqli_query($connection, $query);
    if ($results) {
        $id = mysqli_insert_id($connection);

        foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `lecturer` SET  `$column`='$value' WHERE `id`='$id'");
        }
        return true;
    } else {
        return false;
    }
}
function lecturer_edit($connection, $request,$id)
{
     
    
    
        
                                    
       foreach ($request as $column => $value) {
            mysqli_query($connection, "UPDATE `lecturer` SET `$column`='$value' WHERE `id`='$id'");
        }
        
        return false;
                                }
      
    
    
              

function  lecturer_getcount($connection, $data)
{

    $query = "SELECT * FROM `lecturer` WHERE";
    foreach ($data as $column => $value) {
        $query .= "`$column`='$value' AND ";
    }

    $query = rtrim($query, "AND ");
    $result = mysqli_query($connection, $query);
    $count = mysqli_num_rows($result);

    return $count;
}
 