<?php
include '../theme/header.php';
include '../../controle/db.php';
?>


   <div class="row">
       
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
         
          <div class="small-box bg-aqua">
             
            <div class="inner">
                
                   <?php
                    $query_staff = mysqli_query(connect(), "SELECT * From users where status = 'active' && user_type != 'SA' ");
                    $count_staff = mysqli_num_rows($query_staff);
                    ?>

                 

              <h3><?= $count_staff ?></h3>

              <p>Stafs</p>
            </div>
                <a href="../users/user_table.php">  
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
                    </a>
              <a href="../users/user_table.php" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        
        </div>
        <!-- ./col -->
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
                
                        
<?php
$query_sub_course = mysqli_query(connect(), "SELECT * FROM lecturer where status = 'active' ");
$count_sub_course = mysqli_num_rows($query_sub_course);
?>
                  

              <h3><?= $count_sub_course ?></h3>

              <p>Lectures</p>
            </div>
              <a href="../lecturer/lecturer.php">  
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
</a>
              <a href="../lecturer/lecturer.php" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
                
                <?php
$query_Students = mysqli_query(connect(), "SELECT * From user where status='active' ");
$count_Students = mysqli_num_rows($query_Students);
?>
                  

              <h3><?= $count_Students ?></h3>

              <p>Students</p>
            </div>
              <a href="../student/stdent_details.php">  
            <div class="icon">
              <i class="ion ion-person"></i>
            </div>
              </a>
              <a href="../student/stdent_details.php" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
                <?php
$query_course = mysqli_query(connect(), "SELECT DISTINCT course_code FROM course");
$count_course = mysqli_num_rows($query_course);
?>
                   

              <h3><?= $count_course ?></h3>

              <p>Courses</p>
            </div>
              <a href="../course/course.php">  
            <div class="icon">
              <i class="ion ion-edit"></i>
            </div>
              </a>
              <a href="../course/course.php" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        
      </div>

  



    <div class="col-md-6 col-md-offset-3">

        <!-- DONUT CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h2 class="box-title">  Course Student Chart </h2>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>

                </div>
            </div>
            <div class="box-body">









                <canvas id="pieChart" style="height:250px">


                </canvas>



            </div>
            <div class="box-footer with-border">
                <ul class="nav nav-tabs">
<?php
$colour = "";
$query_c = mysqli_query(connect(), "SELECT DISTINCT course_code FROM course");
while ($row_u = mysqli_fetch_array($query_c)) {
    $course = $row_u["course_code"];

    $query1 = mysqli_query(connect(), "SELECT colour from course where course_code='$course' ");
    $row_c = mysqli_fetch_array($query1);

    $colour = $row_c["colour"];
    if ($colour == "") {
        $colour = "#00a65a";
    }
    ?>

                        <li ><i class="fa fa-circle-o " style="color:  <?= $colour ?>; "></i> <?= $row_u["course_code"]; ?></li>
                        &nbsp;



                    <?php } ?>

                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->


    </div>




<?php
include '../theme/footer.php';
?>




    <script>
        $(function () {


            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            var pieChart = new Chart(pieChartCanvas)
            var PieData = [
<?php
$query_c = mysqli_query(connect(), "SELECT DISTINCT course_code FROM course");
while ($row_u = mysqli_fetch_array($query_c)) {
    $course = $row_u["course_code"];



    $query = mysqli_query(connect(), "SELECT course FROM stu_course where course='$course' ");
    $count = mysqli_num_rows($query);
    $query1 = mysqli_query(connect(), "SELECT colour from course where course_code='$course' ");
    $row_c = mysqli_fetch_array($query1);

    $colour = $row_c["colour"];
    if ($colour == "") {
        $colour = "#00a65a";
    }
    ?>

                    {
                        value: <?= $count ?>,
                        color: '<?= $colour ?>',
                         highlight: '<?= $colour ?>',
                        label: '<?= $row_u["course_code"] ?> students'
                    },
<?php } ?>
            ]
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: '#fff',
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: 'easeOutBounce',
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template
                legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            pieChart.Doughnut(PieData, pieOptions)

            //-------------
            //- BAR CHART -
            //-------------



        })


    </script>