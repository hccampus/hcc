<!DOCTYPE html>
<?php

include '../theme/header.php';
include '../../controle/db.php';
  $page = "My hospital";
  

$query = mysqli_query(connect(), "SELECT * from my_campus");
$row1 = mysqli_fetch_array($query);


?>

 <style>
    #output{

        width: 100px;

        height: 100px;
        background: #007fff;
        border-radius: 20px;

    }



    input#file {
        opacity: 0.0000001;

        width: 100px;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden;

        background: #007fff;
        border-radius: 1px;
        background-size: 1px 1px;

        margin-bottom: 10px; 
        margin-top: -120px; 
        margin-left: 40px;
    }
    input#file:hover {


        opacity: 0.99;

        width:100px!important;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden!important;

        background: url('../../src/bower_components/Ionicons/png/512/ios7-camera-outline.png') center center no-repeat;
        border-radius: 10px;
        background-size: 100px 100px;

       

        margin-left: 0px;
        margin-right: 0px;

    }

    
    
</style>

<section class="content">
    
<div class="row">
    <div class="col-md-8">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Campus Details</h3>
            </div>
              <form method="post" action="../../model/company/campus_profile.php"  >
              <div class="box-body">
                
                   <div class="col-md-6">
                <div class="form-group">
                 <label class="col-sm-4 control-label">Company Name</label>

                  <div class="col-sm-8">
                       <input type="text" class="form-control" name="company_name" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="<?php echo $row1["company_name"] ?>"required="">
                  </div>
                </div>
                   </div>
                   <div class="col-md-6">
                <div class="form-group">
                
 <label class="col-sm-4 control-label" >Short Name</label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" name="s_name" maxlength="4" value="<?php echo $row1["s_name"] ?>"required="">
                  </div>
                </div>
                       
                  </div>
                  
                  <div class="col-md-12">
                  <span for="" class="col-sm-4 control-label"><b>Adress</b></span>
                  </div>
                   <div class="col-md-6">
                <div class="form-group">
                
<label for="" class="col-sm-4 control-label"><i>line 1</i></label>
                  <div class="col-sm-8">
                       <input type="text" class="form-control" name="adress_1"  value="<?php echo $row1["adress_1"] ?>"required="">
                  </div>
                </div>
                   </div>
                  
                  <div class="col-md-6">
                <div class="form-group">
                <label for="" class="col-sm-4 control-label" ><i>line 2</i></label>

                  <div class="col-sm-8">
                     <input type="text" class="form-control" name="adress_2"  value="<?php echo $row1["adress_2"] ?>"required="">
                  </div>
                </div>
                  </div>
                  <div class="col-md-12"><br></div>
                  <div class="col-md-12">
                  <span for="" class="col-sm-4 control-label"><b> </b></span>
                  
                  </div>
                  
                  <div class="col-md-6">
                <div class="form-group">
                 <label for="" class="col-sm-4 control-label" >Mobile No</label>

                  <div class="col-sm-8">
                   <input type="text" class="form-control"  name="phone_no" maxlength="10" value="<?php echo $row1["phone_no"] ?>">
                  </div>
                </div>
                       
                   </div>
                  <div class="col-md-6">
                <div class="form-group">
                  <label for="" class="col-sm-4 control-label">Email</label>

                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="email" pattern="[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?php echo $row1["email"] ?>">
                  </div>
                </div>
                   </div>
                
                
          
              </div>
              <div class="box-footer">
                
                  <button type="submit"  class="btn btn-primary pull-right" name="update_btn"> Update Profile</button>
              </div>
                </form>
          </div>
         
        </div>
      
        <div class="col-md-4">
         <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Campus logo</h3>
             
            </div>
             <form method="post" action="../../model/company/campus_profile.php"  enctype="multipart/form-data">
             <div class="box-body">
                
                 
                            <div class="col-md-6">
                <div class="form-group">
                    
                    
                      <div class="col-sm-7">
                          <img id="output" src="../../model/company/image/<?php echo $row1["image"] ?>"   >
                            <input id="file" onchange="loadFile(event)" type="file" name='image' >
                      </div>
                <script>
                            var loadFile = function (event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                </div>
                            
                      
                      
                      </div>
                     
                  </div>
                   
                   
                 
           <br>
                      <br>
                       <br>
                        <br>
                        
             <div class="box-footer">
                
              <button  type="submit"  class="btn btn-primary pull-right"     name="update_img" > Update Photo</button>
              </div>
             </form>
         </div>
             
          </div>
       
           
        </div>
    
   
    
</section>


<?php
include '../theme/footer.php';

                          