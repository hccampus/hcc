
<?php
$Teachers_activ = "active";
include '../theme/header.php';
include '../../controle/db.php';


$sql = "SELECT *  FROM lecturer where status='active'  ";
$sql_d = "SELECT *  FROM lecturer where status='deactive'  ";
$query_u = mysqli_query(connect(), $sql);  
$query_d = mysqli_query(connect(), $sql_d);
$total_d = mysqli_num_rows($query_d);
$total_u = mysqli_num_rows($query_u);
?> 
<style>
    #output{

        width: 100px;

        height: 100px;
        background: #007fff;
        border-radius: 20px;

    }



    input#file {
        opacity: 0.0000001;

        width: 100px;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden;

        background: #007fff;
        border-radius: 1px;
        background-size: 1px 1px;

        margin-bottom: 10px; 
        margin-top: -120px; 
        margin-left: 40px;
    }
    input#file:hover {


        opacity: 0.99;

        width:100px!important;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden!important;

        background: url('../../src/bower_components/Ionicons/png/512/ios7-camera-outline.png') center center no-repeat;
        border-radius: 10px;
        background-size: 100px 100px;

       

        margin-left: 0px;
        margin-right: 0px;

    }

    
    
</style>
<div class="col-md-12">
    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#all" data-toggle="tab"><button class="btn btn-primary " type="button" >All Lecturer </button></a></li>
               <?php if ($_SESSION['user_type'] == 'SA'){ ?>
                 <li ><a href="#deleted" data-toggle="tab"><button class="btn btn-danger " type="button" >Deleted Lecturer </button></a></li>
                 
               <?php } ?>
            </ul>
    

 <div class="tab-content">
              <div class="active tab-pane" id="all">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Lecturer Details</h3>
          <button class="btn btn-warning  pull-right "data-toggle="modal" data-target="#add_staf" type="button" >Add new lecturer</button>
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table  class="table table-bordered table-striped data">
                <thead>


                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        
                         <th>Actions </th>
                    </tr>
                </thead>
                
               
              
                
  
<?php
while ($row_u = mysqli_fetch_array($query_u)) {
    ?>
                        <tr>
                            <td><img src="../../model/lecturer/lecturer_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                            <td><?= $row_u['id'] ?></td>
                            <td><?=  ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                           
                            <td><?= $row_u['nic'] ?></td>
                             <td><?= $row_u['email'] ?></td>
                              <td><?= $row_u['address'] ?></td>
                                 


                            <td>
                                <br>
                                
                                <button class="btn btn-primary btn-xs  edit" id="<?= $row_u['id'] ?>"  >edit</button>
                                <?php if ($_SESSION["user_type"] != "user"){ ?>
                                <button class="btn btn-danger btn-xs  action" id="<?= $row_u['id'] ?>" value="deactive">delete</button>
                                <?php } ?>  

                            </td>

                        </tr>
                  
<?php } ?>
               
              
                            
                </tbody>
                <tfoot>
                    <tr>
                         <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                       
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 
   
      <div class="tab-pane" id="deleted">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Lecturer Details</h3>
          
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
             <table  class="table table-bordered table-striped data">
                <thead>


                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        
                         <th>Actions </th>
                    </tr>
                </thead>
                
               
              
                
  
<?php
while ($row_u = mysqli_fetch_array($query_d)) {
    ?>
                        <tr>
                            <td><img src="../../model/lecturer/lecturer_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                            <td><?= $row_u['id'] ?></td>
                            <td><?=  ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                           
                            <td><?= $row_u['nic'] ?></td>
                             <td><?= $row_u['email'] ?></td>
                              <td><?= $row_u['address'] ?></td>
                                 


                            <td>
                                <br>
                                
                                <button class="btn btn-primary btn-xs  edit" id="<?= $row_u['id'] ?>"  >edit</button>
                                <button class="btn btn-success btn-xs  action" id="<?= $row_u['id'] ?>" value="active">Active</button>
                                <button class="btn btn-danger btn-xs  delete" id="<?= $row_u['id'] ?>" value="delete">Delete</button>


                            </td>

                        </tr>
                  
<?php } ?>
               
              
                            
                </tbody>
                <tfoot>
                    <tr>
                         <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                     
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 </div>
    </div>
</div>
<!-- /.tab-pane -->

<!-- /.tab-pane -->

<!-- /.tab-pane -->


<div id="add_staf" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="staf_form" action="../../model/lecturer/lecturer_create.php"  enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> Add new Staf  </h4>
                   
                </div>
                 <div class="box box-primary">
    
    <div class="box-body">
 
 <div class="col-md-12">
     <center>
                <div class="form-group">
                    
                     <label for="inputEmail3" class="  control-label">Image </label>
                     <br>
              <img id="output"   />

                        <input type="file" id="file" class="form-control" onchange="loadFile(event)" name="image" required>
                      
                <script>
                            var loadFile = function (event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        
                      
                </div>
                       </center>     
                      
                      </div>
        <div class="row">
        <div class ="col-md-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-4 control-label">Firstname  </label>
                         <div class="col-md-8">
                              <input type="text" name="firstname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="" required="">
                         </div>
                       
                    </div>
                </div>

                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Lastname</label>
                         <div class="col-md-8">
                              <input type="text" name="lastname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="">
                         </div>
                       
                    </div>
                    
                </div>
          
        </div>
        <div class="col-md-12"><br></div>
        
        
        
        
      
        <div class="row">
            
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Address</label>
                         <div class="col-md-8">
                             <input type="text" name="address" class="form-control" value="">
                         </div>
                         
                    </div>
                </div>
            
            <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Mobile</label>
                         <div class="col-md-8">
                             <input type="text" name="mobile" class="form-control" maxlength="10" value="">
                         </div>
                         
                    </div>
                </div>
           
                
        </div>
              
            <div class="col-md-12"><br></div> 
            <div class="row">
                
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">NIC No </label>
                         <div class="col-md-8">
                              <input type="text" pattern="^(?:0?[0-9]{9}[V]|[0-9]{12})$" class="form-control" name="nic" title="xxxxxxxxxxxx or xxxxxxxxxV" maxlength="12" value="" required="">
                         </div>
                        
                    </div>
                </div>
                
                <div class ="col-md-6">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">E-mail </label>
                         <div class="col-md-8">
                             <input type="email" class="form-control" name="email" pattern="[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="">
                         </div> 
                         
                    </div>
                </div>
        
            </div>

      
    </div>

<div class="box-footer">
 <div id='print' style="color: red"></div>
</div>
</div>
                <div class="modal-footer">
                    <button   class="btn btn-primary pull-left add_staf" >Add</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="staf_update" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="staf_up" action="../../model/lecturer/lecturer_edit.php" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> More Details</h4>
                </div>
                <div class="modal-body">
                    <div  class="box box-primary">
                        <div class="box-body">

                            <div id="staf_edit"></div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button   class="btn btn-primary pull-left staf_update">Update</button>
                    <button type="button" class="btn btn-default pull-leftl" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.data').DataTable({

        });
        
          $(document).on('submit', '#staf_form', function(){
              
              $(".add_staf").addClass("hidden");
              
          });
         $(document).on('click', '.edit', function(){
        var id = $(this).attr("id");
        var btn_action = 'update';
        $.ajax({
            url:"../../model/lecturer/view_lecturer.php",
            method:"POST",
            data:{id:id, btn_action:btn_action},
            success:function(data){
                $('#staf_update').modal('show');
                $('#staf_edit').html(data);
            }
        })
    });
    
    $(document).on('click', '.action', function(){
        var id = $(this).attr("id");
        var val = $(this).val();
        var btn_action = 'status';
        if(confirm("Do you want make action on this lecturer?"))
        $.ajax({
            url:"../../model/lecturer/view_lecturer.php",
            method:"POST",
            data:{id:id, btn_action_sta:btn_action, status: val},
            success:function(){
                window.location = "lecturer.php";
            }
        })
    });
        
            $(document).on('click', '.delete', function(){
        var id = $(this).attr("id");
        var val = $(this).val();
        var btn_action = 'status';
        if(confirm("Do you want permanantly delete this lecturer ?"))
        $.ajax({
            url:"../../model/lecturer/view_lecturer.php",
            method:"POST",
            data:{id:id, btn_action_delete:btn_action, status: val},
            success:function(){
                window.location = "lecturer.php";
            }
        })
    });
        
    });
    
   
    
</script>

<?php include '../theme/footer.php'; ?>