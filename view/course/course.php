<?php

$course_active = "active";


include '../theme/header.php';
include '../../controle/db.php';


  if ($_SESSION["user_type"] == "admin" || $_SESSION["user_type"] == "SA" ){
       
   }else{
    ?>
<script>
window.location = "../../index.php"
</script>

<?php
    }
?>
<div class="box box-info box-solid">
    <div class="  box-header">





        <div class=" box-title">

            <button class="btn btn-warning " id="add" value="course">Add New Course </button>

        </div>
    </div>
    <div class=" box-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped data "width="100%">
                <thead>
                    <tr>
                        <th width="5%"></th>
                        <th width="20%">Course Title</th>
                        <th width="20px">Course</th>
                        <th width="20px">Fee</th>
                        <th width="30px">Actions</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th></th>
                        <th>Course Title</th>
                        <th>Course</th>
                        <th>Fee</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class=" box-footer">

    </div>
</div>

<div id="course_add" class="modal">
    <div class="modal-dialog">
        <form method="post" id="new_cou">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> Add New course</h4>

                </div>
                <div class="modal-body">
                    <div  class="box box-primary">
                        <div class="box-body">
                            <div id="add_cou">
                            </div>

                        </div>
                      
                    </div>
                    <div class="modal-footer">
                        <button   class="btn btn-primary pull-left cou_new_add" name="submit" value="submit">Submit</button>
                        <button type="button" class="btn btn-default pull-leftl" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </form>
    </div>


    <script>
        $(document).ready(function () {
            var productdataTable = $('.data').DataTable({

                "ajax": {
                    url: "../../model/ajax/course_tableview.php",
                    type: "POST"
                },
            });
            
          

            $(document).on('click', '#add', function () {
                 productdataTable.ajax.reload();
                var course = $(this).val();
                $.ajax({
                    url: "../../model/ajax/course_mod.php",
                    method: "POST",
                    data: {course: course},
                    success: function (data) {
                        $('#add_cou').html(data);
                        $('#course_add').modal('show');
                    }
                })

            });
             $(document).on('click', '#edit', function () {
                 productdataTable.ajax.reload();
                var course = $(this).val();
                $.ajax({
                    url: "../../model/ajax/course_mod.php",
                    method: "POST",
                    data: {edit: course},
                    success: function (data) {
                        $('#add_cou').html(data);
                        $('#course_add').modal('show');
                    }
                })

            });

            
            $(document).on('submit', '#new_cou', function (event) {
                event.preventDefault();

                var form_data = $(this).serialize();
   
              
                $.ajax({
                    url: "../../model/course/course.php",
                    method: "POST",
                    data: form_data,
                    dataType: 'json',
                    success: function (data)

                    {

                        if (data.msg == "") {
                        
                            $('#course_add').modal('hide');
                            productdataTable.ajax.reload();
                        } else {
                            $('#err').html(data.msg);
                            $('#sub_cou').val("");
                            $('#sub_cou').css('border-color', '#e41212')

                        }

                    }
                })
            });
            
             $(document).on('change', '#sub_cou', function () {

                var course_code = $('#Course_code').val();
                var course_name = $(this).val();
                var checked = "check";
                $.ajax({
                    url: "../../model/course/course.php",
                    method: "POST",
                    data: {course_code: course_code, course_name: course_name, check: checked},
                    dataType: 'json',
                    success: function (data)

                    {

                        if (data.msg == 0) {
                            $('#err').html("");
                            $('#sub_cou').css('border-color', '')
                        } else {
                            $('#err').html(data.msg);
                            $('#sub_cou').val("");
                            $('#sub_cou').css('border-color', '#e41212')
                        }

                    }
                })
            });
            
             $(document).on('click', '#delete', function (event) {
                event.preventDefault();

                var id = $(this).val();
   if(confirm("Please Confirm do you want delete this course.!!"))
              
                $.ajax({
                    url: "../../model/course/delete_cou.php",
                    method: "POST",
                    data:{id: id},
                    success: function ()

                    {
                       
                        productdataTable.ajax.reload();
                    }
                })
            });
            
        });
    </script>


    <?php include '../theme/footer.php'; ?>
