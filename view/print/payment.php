    <?php
session_start();
include '../../controle/db.php';
$id=0;
$location = 0;
if (isset($_GET['stu_newid'])){
    $id = $_GET['stu_newid'];
    $location = $_GET['location'];
    $query = mysqli_query(connect(), "SELECT * FROM payments where id='$id' ");
$row = mysqli_fetch_array($query);
$stuid = $row['stu_id'];
$query_user = mysqli_query(connect(), "SELECT * FROM user where stu_id='$stuid'");
$row_user = mysqli_fetch_array($query_user);
}else{
    $location = $_GET['location'];
    $query = mysqli_query(connect(), "SELECT * FROM payments  ORDER BY id DESC limit 1 ");
$row = mysqli_fetch_array($query);
$id = $row['stu_id'];
$query_user = mysqli_query(connect(), "SELECT * FROM user where stu_id='$id'");
$row_user = mysqli_fetch_array($query_user);
}
  
?>



<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HCC Student | Details</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../../src/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../../src/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../../src/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../src/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../../src/plugins/iCheck/square/_all.css">
        <link rel="stylesheet" href="print.css">


        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <style>
        .box{
            border-radius: 5px;
        }
        .well{
            border-radius: 5px;
        }
        .invoice{
            border: 1px solid #000;  
            padding: 10px;
        }

        .foo {

            width:30px;
            height:30px;
            border: 2px solid #000;
            display: inline-block;
            margin-top: -5px !important;

            border-color: #000 !important;
            margin-right: 50px;
        }
        /* This is what simulates a checkmark icon */
        .foo.checked:after {
            content: '';
            display: block;
            width: 6px !important;
            height: 10px !important;

            /* "Center" the checkmark */
            position:relative;
            top:4px !important;
            left:6px !important;

            border: solid #000 !important;
            border-width: 0 2px 2px 0 !important;
            transform: rotate(45deg) !important;
        }
.invoice {
     border: 0; 
    padding-left: 20px; 
     padding-right: 20px; 
    
    margin-top: -10px;
}

.first{
    border: 1px solid #000 !important;
    height: 306px !important;
    background: #3c8dbc !important;
     padding: 1px !important;
}
.second{
   
    height: 300px !important;
    margin-left: 1px !important;
       
  
     background: white !important;
    
}
.third{
   
    height: 300px !important;
    margin-left: -16px !important;
  
  
     background: white !important;
}
.heade_des{
    border-color: #000;
    height: 50px;
    background: #3c8dbc !important;
    border-radius: 0px 0px 50px 0px;
    width: 300px;
    margin-left: -16px;
    margin-top: 0px; 
}

.foo_des{
    border-color: #000;
    height: 20px;
    background: #3c8dbc !important;
    border-radius: 0px 50px 0px 0px;
    width: 200px;
    margin-left: -16px;
    margin-top: 230px; 
}


.rotateObj {
    
    width:300px  !important;
    display:inline-block  !important;
    position:absolute  !important;
    left:600px  !important;
    bottom: 160px  !important;
    -webkit-transform:rotate(-90deg)  !important;
    color: white !important;
    font-size: 30px  !important;
  
}

.date_pay{
  
    width: 130px !important;;
  
 
    margin-left: 650px !important;
    margin-top: -280px !important;
}

.sign_pay{
  
    width: 150px !important;;
  
 
    margin-left: 500px !important;
    margin-top: 250px !important;
}
.aut_pay{
  
    width: 130px !important;;
  
 
    margin-left: 510px !important;
    margin-top: 20px !important;
}





    </style>

    <script>
window.onafterprint = function(e){
    closePrintView();
};

function myFunction(){
    window.print();
}

function closePrintView() {
    window.location = "<?= $location ?>";   
}
    
    </script>
    
    <body style="font-family: serif;line-height: 0; " onload="myFunction();" >

        <!-- Main content -->
        <section class="invoice" >

        
          
            <div class="row" >
                <div class="col-sm-12  first" >
                  <div class="col-sm-12  second" >
                    <div class="col-sm-12  third"   >
                        
                              
                        <div class="box-header heade_des" >
                           
                        
                        <table>
                            <tr>
                            <td> <img src="../../model/company/image/<?= $_SESSION['company_image'] ?>" style="width: 30px; height:  30px;margin: -1px"></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td> <span style="font-size: 18px;margin: 8px;color: white !important"><?= $_SESSION['company_name'] ?></span><br>
                                
                            </td>
                            </tr>
                          
                            <tr>
                                <td> </td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            
                            <td> 
                                
                                <span style="font-family: sans-serif;font-size: 8px;color: white !important;"><center style='color: white !important;'><?= $_SESSION["adress_1"],", ".$_SESSION["adress_2"],", ".$_SESSION["phone_no"]."."?></center></span>
                            </td>
                            </tr>
                        </table>
                            </div>
                          
                           
                      
                 
                       <div class="box-header foo_des" >
                             <p style="color: white !important">HOTLINE : 071-764-0411</p>
                       </div>
                        
                      
                        
                         <div class="date_pay">
                                
                                <div class=" pull-right" style="margin-right: 150px  !important; width: 100% !important;"><b>Date : </b></div>
                          
                            
                                <div class="foo wine" style="margin-right: 100px  !important; width: 100% !important; height: 20px !important;padding: 10px !important;"><center><?= substr($row['created_at_fee'], 0,10) ?></center></div>
                          
                     
                            </div>
                     
                         <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 50px !important;">
                            <div class="col-xs-3" ><b> Receipt No  </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 200px  !important; width: 150px !important;padding: 10px !important;"><?= $row['rct_no'] ?></div>
                            </div>
                        </div>
                    
                         <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 10px !important;">
                            <div class="col-xs-3" ><b> Admission No  </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 200px  !important; width: 150px !important;padding: 10px !important;"><?= $row['stu_id'] ?></div>
                            </div>
                        </div>
                         
                          <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 10px !important;">
                            <div class="col-xs-3" ><b> Name   </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 50px  !important; width: 300px !important;padding: 10px !important;"><?= $row_user['firstname'] ?></div>
                            </div>
                        </div>
                          <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 10px !important;">
                            <div class="col-xs-3" ><b>Course  Name  </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 50px  !important; width: 300px !important;padding: 10px !important;"><?= $row['course'],"-". $row['sub_cou'] ?></div>
                            </div>
                        </div>
                            <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 10px !important;">
                            <div class="col-xs-3" ><b>Amount </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 200px  !important; width: 150px !important;padding: 10px !important;"><?= number_format($row['payments'],2) ?></div>
                            </div>
                        </div>
                              <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12" style=" margin-top: 10px !important;">
                            <div class="col-xs-3" ><b> Installment/Month </b></div>:&nbsp;&nbsp;
                            <div class="col-xs-9 invoice-col10" >
                                <div class="foo wine" style="margin-right: 200px  !important; width: 150px !important;padding: 10px !important;"><?= $row['pay_order'] ?></div>
                            </div>
                        </div>
                                    <div class="sign_pay">
                            <center>
                             
                            
                            
                                 <u>  &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;    
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                  &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                 &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                  &nbsp;&nbsp;&nbsp;&nbsp;

                            </u>
                             
                            </center>
                          
                        </div>
                                <div class="aut_pay">
                            <center>
                             

                                <b style="color: #3c8dbc !important;" >  Athorized Signature</b>
                            </center>
                          
                        </div>
                    </div>
                </div>  
                       
               
                            
                    <h2 class="rotateObj" style="color: #3c8dbc !important;">PAYMENT RECIEPT</h2>
  
             
            </div> 
            
            
       
    
            

            
        





        </section>
        
      

      


</body>
<script src="../../src/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../src/plugins/iCheck/icheck.min.js"></script>
<script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-red',
                radioClass: 'iradio_square-red',
                increaseArea: '20%'
            });
        });
        
     
</script>
<script type="text/javascript">
    $(document).ready(function () {
var al = $( "a:last-child").html("");

});
</script>
</html>
