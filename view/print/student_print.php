<?php
session_start();
include '../../controle/db.php';
$id=0;
if (isset($_GET['stu_newid'])){
    $id = $_GET['stu_newid'];
    $location = $_GET['location'];
}
?>



<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HCC Student | Details</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="../../src/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../../src/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../../src/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../../src/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../../src/plugins/iCheck/square/_all.css">
        <link rel="stylesheet" href="print.css">


        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <style>
        .box{
            border-radius: 5px;
        }
        .well{
            border-radius: 5px;
        }
        .invoice{
            border: 1px solid #000;  
            padding: 10px;
        }

        .foo {

            width:30px;
            height:30px;
            border: 2px solid #000;
            display: inline-block;
            margin-top: -5px !important;

            border-color: #000 !important;
            margin-right: 50px;
        }
        /* This is what simulates a checkmark icon */
        .foo.checked:after {
            content: '';
            display: block;
            width: 6px !important;
            height: 10px !important;

            /* "Center" the checkmark */
            position:relative;
            top:4px !important;
            left:6px !important;

            border: solid #000 !important;
            border-width: 0 2px 2px 0 !important;
            transform: rotate(45deg) !important;
        }

    </style>

    <script>
window.onafterprint = function(e){
    closePrintView();
};

function myFunction(){
    window.print();
}

function closePrintView() {
    window.location = "<?= $location ?>";   
}
    
    </script>
    

    <body style="font-family: serif;border: #000; " onload="myFunction();">

        <!-- Main content -->
        <section class="invoice" >

            <div class="row">
                <div class="col-sm-10 invoice-col70">
                    <div class="text-muted well well-sm no-shadow" style="border-color: #000;height: 100px">
                        <table>
                            <td> <img src="../../model/company/image/<?= $_SESSION['company_image'] ?>" style="width: 75px; height:  75px;margin: 2px"></td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td> <span style="font-size: 27px;margin: 25px"><?= $_SESSION['company_name'] ?></span><br>
                                <span style="font-family: sans-serif;font-size: 10px"><center><?= $_SESSION['company_title'] ?></center></span>
                            </td>
                        </table>
                    </div>

                </div>
                <div class=" col-sm-2 invoice-col30">
                    <div class="box box-primary box-solid" style="border-color: #000;height: 100px">
                        <div class="box-header with-border" style="border-color: #000;height: 50px">
                            <h4 style="font-family: serif;color: white !important">
                                <center>
                                    <strong style="color: white !important">Application Number</strong>
                                </center>
                            </h4>
                        </div>
                        <div class="box-body with-border" >
                            <center>
                                <b><?= $id ?></b>
                            </center>
                        </div>


                    </div>
                </div>
            </div> 
            <div class="row" style="margin-top: -10px">
                <div class="col-sm-12 ">

                    <div class="box box-primary box-solid" style="border-color: #000;height: 30px;background: #3c8dbc !important;   ">

                        <center><h4 style="font-family: serif;color: white !important;"><strong style="color: white !important">APPLICATION FORM</strong></h4></center>
                    </div>


                </div>
            </div> 
            <div class="row" style="margin-top: -10px">
                <div class="col-sm-10 invoice-col10" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 150px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Programme Title</strong>
                        </div>
                        <br>
                        <?php
                        $check = 0;
                        $query = mysqli_query(connect(), "SELECT DISTINCT course_code from course");
                        while ($row = mysqli_fetch_array($query)) {

                            $query_vou = mysqli_query(connect(), "SELECT * FROM stu_course where stu_id='$id'");
                            $row_cou = mysqli_fetch_array($query_vou);
                            if ($row['course_code'] == $row_cou['course']) {
                                $check = "checked";
                            }else{
                                $check = '';
                            }
                            ?>

                            <div class="col-xs-1 invoice-col20" style="padding:10px  !important;">
                                <strong style="font-size: 13px;"><?= $row['course_code'] ?></strong><div class="foo <?= $check ?>" style="margin-right: 15px  !important;"></div>
                            </div>


<?php }
?>

                    </div>
<?php
$query_user = mysqli_query(connect(), "SELECT * FROM user where stu_id='$id'");
$row_user = mysqli_fetch_array($query_user);
?>

                </div>
                <div class="col-sm-2 invoice-col20 ">

                    <div class="text-muted " style="border-color: #000;height: 35px;border-top-color: #000"   >
                        <b>Student Image  </b>

                    </div>
                    <div class="text-muted well well-sm" style="border-color: #000;height: 95px;border-top-color: #000"   >

                        <img src="../../model/student/Student_img/<?= $row_user['image'] ?>" style="border-top-color: #000;border-radius: 5px;height: 100%;width: 100%;">
                    </div>



                </div>
            </div> 

            <div class="row" style="margin-top: -10px">
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 480px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 0px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Personal Information</strong>
                        </div>
                        <br>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Title</b></div>
                            <div class="col-xs-1 invoice-col1" >
                                Mr.<div class="foo <?php if ($row_user['title'] == 'mr') {
                        echo 'checked';
                    } ?>" style="margin-right: -25px  !important; "></div>
                            </div>
                            <div class="col-xs-1 invoice-col1" >
                                Mrs.<div class="foo  <?php if ($row_user['title'] == 'mrs.') {
                        echo 'checked';
                    } ?>" style="margin-right: -25px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col1" >
                                Miss.<div class="foo  <?php if ($row_user['title'] == 'ms') {
                        echo 'checked';
                    } ?>" style="margin-right: -25px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col1" >
                                Rev.<div class="foo  <?php if ($row_user['title'] == 'rev') {
                        echo 'checked';
                    } ?>" style="margin-right: -25px  !important;"></div>
                            </div> 

                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>First Name</b></div>
                            <div class="col-xs-10 invoice-col10" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['firstname'] ?></div>
                            </div>

                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Last Name</b></div>
                            <div class="col-xs-10 invoice-col10" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['lastname'] ?></div>
                            </div>

                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>NIC</b></div>
                            <div class="col-xs-5 invoice-col30" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['nic'] ?></div>
                            </div>
                            <div class="col-xs-3 invoice-col20" >
                                <div class=" pull-right" style="margin-right: -40px  !important; width: 100% !important;"><b>Date of Birth</b></div>
                            </div>
                            <div class="col-xs-2 invoice-col30" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['dateofbirth'] ?></div>
                            </div>

                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Address</b></div>
                            <div class="col-xs-10 invoice-col10" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= strtoupper($row_user['line1']), " ," . strtoupper($row_user['line2']) ?></div>
                            </div>
                            <div class="col-xs-2" ></div>
                            <div class="col-xs-10 invoice-col10" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= strtoupper($row_user['city']), " ," .strtoupper($row_user['country']) ?></div>
                            </div>

                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Gender</b></div>
                            <div class="col-xs-1 invoice-col15" >
                                Male<div class="foo <?php if ($row_user['gender'] == 'male') {
                        echo 'checked';
                    } ?>" style="margin-right: -10px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col15" >
                                Female<div class="foo <?php if ($row_user['gender'] == 'female') {
                        echo 'checked';
                    } ?>" style="margin-right: -10px  !important;"></div>
                            </div>
                            <div class="col-xs-2 invoice-col20" style="margin-right: -40px  !important;" >
                                <b>Civil Status</b>
                            </div>
                            <div class="col-xs-1 invoice-col15" >
                                Single<div class="foo <?php if ($row_user['civilstatus'] == 'single') {
                        echo 'checked';
                    } ?>" style="margin-right: -10px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col15" >
                                Married<div class="foo <?php if ($row_user['civilstatus'] == 'maried') {
                        echo 'checked';
                    } ?>" style="margin-right: -10px  !important;"></div>
                            </div>


                        </div> 
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Contact</b></div>
                            <div class="col-xs-1" ><b>Home</b></div>
                            <div class="col-xs-5 invoice-col40" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"></div>
                            </div>


                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b></b></div>
                            <div class="col-xs-1" ><b>Mobile</b></div>
                            <div class="col-xs-5 invoice-col40" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['mobile'] ?></div>
                            </div>


                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b></b></div>
                            <div class="col-xs-1" ><b>Work</b></div>
                            <div class="col-xs-5 invoice-col40" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['work'] ?></div>
                            </div>


                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b></b></div>
                            <div class="col-xs-1" ><b>Email</b></div>
                            <div class="col-xs-5 invoice-col70" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row_user['email'] ?></div>
                            </div>


                        </div>
                    </div>
                </div>
            </div> 
            <div class="row" style="margin-top: -10px">
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 210px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Academic Qualification</strong>
                        </div>
<?php
$query = mysqli_query(connect(), "SELECT * FROM al where stu_id='$id'");
$row = mysqli_fetch_array($query);
?>
                        <div class="col-xs-5">
                            <div style="padding: 2px"> <b>G.C.E. Advanced Level</b></div>
                            <div class="col-xs-12 invoice-col10" >
                                Year<div class="foo wine" style="margin-right: -25px  !important;width: 150px !important;"><b><?= $row['year_al'] ?></b></div>
                            </div>
                            <div class="col-xs-12 invoice-col10" >
                                Stream<div class="foo wine" style="margin-right: -25px  !important;width: 150px !important;"><b><?= $row['stream'] ?></b></div>
                            </div>
                            <table border="1">
                                <thead>
                                    <tr style="color: white !important;background: #3c8dbc !important">
                                        <th style="width: 180px;color: white !important;">Subject</th>
                                        <th style="width: 80px;color: white !important;">Grade</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td><?= $row['al_subject1'] ?></td>
                                        <td><?php if ($row['al_grade1'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['al_grade1'];
} ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $row['al_subject2'] ?></td>
                                        <td><?php if ($row['al_grade2'] == 'not selected') {
                            echo "Not sit ";
                        } else {
                            echo $row['al_grade2'];
                        } ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $row['al_subject3'] ?></td>
                                        <td><?php if ($row['al_grade3'] == 'not selected') {
                            echo " Not sit";
                        } else {
                            echo $row['al_grade3'];
                        } ?></td>
                                    </tr>

                                    <tr>
                                        <td><?= $row['al_subject4'] ?></td>
                                        <td><?php if ($row['al_grade4'] == 'not selected') {
                            echo "Not sit ";
                        } else {
                            echo $row['al_grade4'];
                        } ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
<?php
$query = mysqli_query(connect(), "SELECT * FROM ol where stu_id='$id'");
$row = mysqli_fetch_array($query);
?>
                        <div class="col-xs-7">
                            <div style="padding: 2px"> <b>G.C.E. Ordinary Level</b></div>
                            <div class="col-xs-12 invoice-col10" >
                                Year<div class="foo wine" style="margin-right: 80px  !important;width: 150px !important;"><b><?= $row['year_ol'] ?></b></div>
                            </div>
                            <div class="col-xs-12 invoice-col12" >
                                <div class="col-xs-6 " > 
                                    <table border="1">
                                        <thead>
                                            <tr style="color: white !important;background: #3c8dbc !important">
                                                <th style="width: 100px;color: white !important;">Subject</th>
                                                <th style="width: 70px;color: white !important;">Grade</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td><b>English</b></td>
                                                <td><?php if ($row['english'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['english'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>History</b></td>
                                                <td><?php if ($row['history'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['history'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Religion</b></td>
                                                <td><?php if ($row['religion'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['religion'];
} ?></td>
                                            </tr>

                                            <tr>
                                                <td><b>Maths</b></td>
                                                <td><?php if ($row['maths'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['maths'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b>Science</b></td>
                                                <td><?php if ($row['science'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['science'];
} ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-6 " > 
                                    <table border="1">
                                        <thead>
                                            <tr style="color: white !important;background: #3c8dbc !important">
                                                <th style="width: 100px;color: white !important;">Subject</th>
                                                <th style="width: 70px;color: white !important;">Grade</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td><b>Language</b></td>
                                                <td><?php if ($row['tamil'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['tamil'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php if ($row['ol_subject7'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_subject7'];
} ?></b></td>
                                                <td><?php if ($row['ol_grade7'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_grade7'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php if ($row['ol_subject8'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_subject8'];
} ?></b></td>
                                                <td><?php if ($row['ol_grade8'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_grade8'];
} ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php if ($row['ol_subject9'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_subject9'];
} ?></b></td>
                                                <td><?php if ($row['ol_grade9'] == 'not selected') {
    echo "Not sit ";
} else {
    echo $row['ol_grade9'];
} ?></td>
                                            </tr>

                                            <tr>
                                                <td><b>&nbsp;</b></td>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> 

                    </div>


                </div>

            </div> 





        </section>

        <!-- /.content -->
        <section class="invoice" >





            <div class="row" >
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 170px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Professional Qualification</strong>
                        </div>
                        <br>
                        <table border="1" style="width: 100%;">
                            <thead>
                                <tr style="color: white !important;background: #3c8dbc !important">
                                    <th style="width: 150px;color: white !important;"><center style="color: white !important;">Year</center></th>
                            <th style="width: 200px;color: white !important;"><center style="color: white !important;">Institute</center></th>
                            <th style="width: 80px;color: white !important;"><center style="color: white !important;">Grade</center></th>
                            </tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>   

                            </tbody>
                        </table>
                    </div> 



                </div>


            </div>
            <div class="row" >
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 190px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Work Experiance</strong>
                        </div>
                        <br>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b>Status</b></div>
                            <div class="col-xs-1 invoice-col20" >
                                Employed<div class="foo " style="margin-right: 10px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col20" >
                                Unemployed<div class="foo " style="margin-right: -5px  !important;"></div>
                            </div>
                            <div class="col-xs-1 invoice-col20" >
                                selfe employed<div class="foo " style="margin-right: -15px  !important;"></div>
                            </div>
                        </div>
                        <br>

                        <div class="col-xs-8">
                            <table border="1" style="width: 100%;">
                                <thead>
                                    <tr style="color: white !important;background: #3c8dbc !important">
                                        <th style="width: 150px;color: white !important;"><center style="color: white !important;">Organization</center></th>
                                <th style="width: 200px;color: white !important;"><center style="color: white !important;">Designation</center></th>

                                </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>   

                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4">
                            <table border="1" style="width: 100%;">
                                <thead>
                                    <tr style="color: white !important;background: #3c8dbc !important">
                                        <th style="width: 150px;color: white !important;"><center style="color: white !important;">From</center></th>
                                <th style="width: 200px;color: white !important;"><center style="color: white !important;">To</center></th>

                                </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>   

                                </tbody>
                            </table>
                        </div>
                    </div> 



                </div>


            </div>
            <div class="row" >
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 400px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Declaration</strong>
                        </div>
                        <br>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <b>1.&nbsp; I shall at all times use acceptable & appropriate language with all studens and <br>
                                &nbsp;&nbsp;&nbsp;&nbsp; lecturers thereby respecting the privarcy and honour of each other.
                            </b>
                        </div> 

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <b>2.&nbsp; I shall at all times uphold the name and honour of Hallam City Campus (Pvt) Ltd, <br>
                                &nbsp;&nbsp;&nbsp;&nbsp; and be an examplary role, in and out of the campus premises.
                            </b>
                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <b>3.&nbsp; I agree to pay all payments or installments,1 month before the tenure of the <br>
                                &nbsp;&nbsp;&nbsp;&nbsp; respective course and will accept any discrete action that would be taken by the<br>
                                &nbsp;&nbsp;&nbsp;&nbsp; management if otherwise.
                            </b>
                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <b>4.&nbsp; I hereby declare that the details furnished above are true and correct to the best <br>
                                &nbsp;&nbsp;&nbsp;&nbsp; of my knowledge and belief and I undertake to inform you of any changes therein,<br>
                                &nbsp;&nbsp;&nbsp;&nbsp; immediatly.
                            </b>
                        </div>

                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-4 invoice-col40">
                            <center>
                                  <u>  &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;    
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;

                            </u>
                            <br>
                            <b>  Date</b>
                            </center>
                          
                        </div>
                        <div class="col-xs-4 invoice-col20">
                            &nbsp;&nbsp;&nbsp;&nbsp; 
                        </div>

                        <div class="col-xs-4 invoice-col40">
                          <center>
                                  <u>  &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;    
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;

                            </u>
                            <br>
                            <b> Signature </b>
                            </center>
                        </div>



                    </div>


                </div>
            </div> 
            <div class="row" >
                <div class="col-sm-12" >
                    <div class="text-muted well well-sm " style="border-color: #000;height: 190px;border-top-color: #000" >
                        <div class="box-header" style="border-color: #000;height: 30px;background: #3c8dbc !important;border-radius: 5px 0px 50px 0px;width: 300px;margin-left: -10px;margin-top: -10px; ">
                            <strong style="color: white !important">Emergency</strong>
                        </div>
<?php
$query = mysqli_query(connect(), "SELECT * FROM emergency where stu_id='$id'");
$row = mysqli_fetch_array($query);
?>
                        <br>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b> Name</b></div>
                            <div class="col-xs-10 invoice-col10" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row['p_name'] ?></div>
                            </div>

                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b> Relationship</b></div>
                            <div class="col-xs-10 invoice-col70" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row['relationship'] ?></div>
                            </div>

                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">

                            <div class="col-xs-2" ><b> Contact No</b></div>
                            <div class="col-xs-10 invoice-col40" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row['mobile_1'] ?></div>
                            </div>

                        </div>
                        <div class="col-xs-12">&nbsp;</div>
                        <div class="col-xs-12">
                            <div class="col-xs-2" ><b> </b></div>
                            <div class="col-xs-10 invoice-col40" >
                                <div class="foo wine" style="margin-right: -1px  !important; width: 100% !important;"><?= $row['mobile_2'] ?></div>
                            </div>

                        </div>

                    </div> 



                </div>


            </div>
        </div> 






    </section>


</body>
<script src="../../src/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../src/plugins/iCheck/icheck.min.js"></script>
<script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-red',
                radioClass: 'iradio_square-red',
                increaseArea: '20%'
            });
        });
</script>

<script type="text/javascript">
    $(document).ready(function () {
var al = $( "a:last-child").html("");

});
</script>
</html>
