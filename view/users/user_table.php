
<?php
$Users_activ = "active";
include '../theme/header.php';
include '../../controle/db.php';

   if ($_SESSION["user_type"] == "admin" || $_SESSION["user_type"] == "SA" ){
       
   }else{
    ?>
<script>
window.location = "../../index.php"
</script>

<?php
    }
?>


<?php
$sql = "SELECT *  FROM users where status='active'  ";
$sql_d = "SELECT *  FROM users where status='deactive'  ";
$query_u = mysqli_query(connect(), $sql);  
$query_d = mysqli_query(connect(), $sql_d);
$total_d = mysqli_num_rows($query_d);
$total_u = mysqli_num_rows($query_u);
?> 
<style>
    #output{

        width: 100px;

        height: 100px;
        background: #007fff;
        border-radius: 20px;

    }



    input#file {
        opacity: 0.0000001;

        width: 100px;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden;

        background: #007fff;
        border-radius: 1px;
        background-size: 1px 1px;

        margin-bottom: 10px; 
        margin-top: -120px; 
        margin-left: 40px;
    }
    input#file:hover {


        opacity: 0.99;

        width:100px!important;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden!important;

        background: url('../../src/bower_components/Ionicons/png/512/ios7-camera-outline.png') center center no-repeat;
        border-radius: 10px;
        background-size: 100px 100px;

       

        margin-left: 0px;
        margin-right: 0px;

    }

    
    
</style>
<div class="col-md-12">
    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#all" data-toggle="tab"><button class="btn btn-primary " type="button" >All Stafs </button></a></li>
                <?php if (($_SESSION['user_type']) == 'SA'){ ?>
                 <li ><a href="#deleted" data-toggle="tab"><button class="btn btn-danger " type="button" >Deleted Stafs </button></a></li>
                <?php } ?>
             
            </ul>
    

 <div class="tab-content">
              <div class="active tab-pane" id="all">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Staf Details</h3>
          <button class="btn btn-warning  pull-right "data-toggle="modal" data-target="#add_staf" type="button" >Add new staf</button>
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table  class="table table-bordered table-striped data" style="width: 100%;">
                <thead>


                    <tr>
                         <th></th>
                        <th>Image</th>
                        
                        <th>FullName</th>
                        <th>User Name</th>
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>User Type</th>
                         <th>Actions </th>
                    </tr>
                </thead>
                
               
              
                
  
  
              
                            
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                         <th>Image</th>
                       
                        <th>FullName</th>
                        <th>User Name</th>
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>User Type</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 
   
      <div class="tab-pane" id="deleted">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Staff Details</h3>
          
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
             <table  class="table table-bordered table-striped data_del" style="width: 100%;">
                <thead>


                    <tr>
                        <th></th>
                        <th>Image</th>
                        
                        <th>FullName</th>
                        <th>User Name</th>
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>User Type</th>
                         <th>Actions </th>
                    </tr>
                </thead>
                
               
              
                
  

               
              
                            
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                         <th>Image</th>
                        
                        <th>FullName</th>
                        <th>User Name</th>
                        <th>NIC</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>User Type</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 </div>
    </div>
</div>
<!-- /.tab-pane -->

<!-- /.tab-pane -->

<!-- /.tab-pane -->


<div id="add_staf" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="staf_form" action="../../model/users/users_create.php"  enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> Add new Staf  </h4>
                   
                </div>
                 <div class="box box-primary">
    
    <div class="box-body">
 
 <div class="col-md-12">
     <center>
                <div class="form-group">
                    
                     <label for="inputEmail3" class="  control-label">Image </label>
                     <br>
              <img id="output"   />

                        <input type="file" id="file" class="form-control" onchange="loadFile(event)" name="image" required>
                      
                <script>
                            var loadFile = function (event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                        
                      
                </div>
                       </center>     
                      
                      </div>
        <div class="row">
        <div class ="col-md-6">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-4 control-label">Firstname  </label>
                         <div class="col-md-8">
                              <input type="text" name="firstname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="" required="">
                         </div>
                       
                    </div>
                </div>

                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Lastname</label>
                         <div class="col-md-8">
                              <input type="text" name="lastname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="">
                         </div>
                       
                    </div>
                    
                </div>
          
        </div>
        <div class="col-md-12"><br></div>
        <div class="row">
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Username </label>
                         <div class="col-md-8">
                             <input type="text" name="username" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="" required="">
                         </div>
                         
                    </div>
                </div>

            
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Usertype</label>
                         <div class="col-md-8">
                              <select name="user_type" id="user_type" class="form-control" required="">
                                  <option hidden="" value="">Usrtype</option>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                            
                        </select>
                         </div>
                        
                    </div>
                </div>
          
        </div>
        
        
        <div class="col-md-12"><br></div> 
         <div class="row">
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Password  </label>
                         <div class="col-md-8">
                             <input type="password" class="form-control" name="password_1" required="">
                         </div>
                         
                    </div>
                </div>
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Confirm password  </label>
                        
                         <div class="col-md-8">
                             <input type="password" class="form-control" name="password_2" required="">
                         </div>
                         
                    </div>
                </div>
            </div>
        <div class="col-md-12"><br></div> 
        <div class="row">
            
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Address</label>
                         <div class="col-md-8">
                             <input type="text" name="address" class="form-control" value="">
                         </div>
                         
                    </div>
                </div>
            
            <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">Mobile</label>
                         <div class="col-md-8">
                             <input type="text" name="mobile" class="form-control" maxlength="10" value="">
                         </div>
                         
                    </div>
                </div>
           
                
        </div>
              
            <div class="col-md-12"><br></div> 
            <div class="row">
                
                <div class ="col-md-6 ">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">NIC No </label>
                         <div class="col-md-8">
                              <input type="text" pattern="^(?:0?[0-9]{9}[V]|[0-9]{12})$" class="form-control" name="nic" title="xxxxxxxxxxxx or xxxxxxxxxV" maxlength="12" value="" required="">
                         </div>
                        
                    </div>
                </div>
                
                <div class ="col-md-6">
                    <div class="form-group">
                         <label for="inputEmail3" class="col-md-4 control-label">E-mail </label>
                         <div class="col-md-8">
                             <input type="email" class="form-control" name="email" pattern="[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="">
                         </div> 
                         
                    </div>
                </div>
        
            </div>

      
    </div>

<div class="box-footer">
 <div id='print' style="color: red"></div>
</div>
</div>
                <div class="modal-footer">
                    <button   class="btn btn-primary pull-left add_staf" >Add</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="staf_update" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="staf_up" action="../../model/users/users_edit.php" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> More Details</h4>
                </div>
                <div class="modal-body">
                    <div  class="box box-primary">
                        <div class="box-body">

                            <div id="staf_edit"></div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button   class="btn btn-primary pull-left staf_update">Update</button>
                    <button type="button" class="btn btn-default pull-leftl" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
            var productdataTable = $('.data').DataTable({

                "ajax": {
                    url: "../../model/users/user_table.php",
                    type: "POST"
                },
                
                
            });
            
               var dataTable_daeactive = $('.data_del').DataTable({

                "ajax": {
                    url: "../../model/users/deleted_user.php",
                    type: "POST"
                },
                });
        
          $(document).on('submit', '#staf_form', function(){
              
              $(".add_staf").addClass("hidden");
              
          });
         $(document).on('click', '.edit', function(){
        var id = $(this).attr("id");
        var btn_action = 'update';
        $.ajax({
            url:"../../model/users/view_users.php",
            method:"POST",
            data:{id:id, btn_action:btn_action},
            success:function(data){
                $('#staf_update').modal('show');
                $('#staf_edit').html(data);
            }
        })
    });
    
    $(document).on('click', '.action', function(){
        var id = $(this).attr("id");
        var val = $(this).val();
        var btn_action = 'status';
        if(confirm("Do you want make action on this staf?"))
        $.ajax({
            url:"../../model/users/view_users.php",
            method:"POST",
            data:{id:id, btn_action_sta:btn_action, status: val},
            success:function(){
                productdataTable.ajax.reload();
                dataTable_daeactive.ajax.reload();
            }
        })
    });
           $(document).on('click', '.delete', function(){
        var id = $(this).attr("id");
        var val = $(this).val();
        var btn_action = 'delete';
        if(confirm("Do you want Permenatly Delete This Staff?")){
        $.ajax({
            url:"../../model/users/view_users.php",
            method:"POST",
            data:{id:id, btn_action_delete:btn_action, status: val},
            success:function(){
                productdataTable.ajax.reload();
                dataTable_daeactive.ajax.reload();
            }
        })
    }
    });
        
  
    });
    
   
    
   
    
</script>

<?php include '../theme/footer.php'; ?>