
<?php

include '../theme/header.php';
include '../../controle/db.php';


?>


<?php
$username = $_SESSION['username'];
$query = mysqli_query(connect(), "SELECT *  FROM users where username='$username'  ") ;
$row = mysqli_fetch_array($query);
$id = $row['id'];
?>
 <style>
    #output{

        width: 100px;

        height: 100px;
        background: #007fff;
        border-radius: 20px;

    }



    input#file {
        opacity: 0.0000001;

        width: 100px;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden;

        background: #007fff;
        border-radius: 1px;
        background-size: 1px 1px;

        margin-bottom: 10px; 
        margin-top: -120px; 
        margin-left: 40px;
    }
    input#file:hover {


        opacity: 0.99;

        width:100px!important;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden!important;

        background: url('../../src/bower_components/Ionicons/png/512/ios7-camera-outline.png') center center no-repeat;
        border-radius: 10px;
        background-size: 100px 100px;

       

        margin-left: 0px;
        margin-right: 0px;

    }

    
    
</style>
<div class="col-md-8">
    <form method="post" id="staf_form" action="../../model/users/user_profile.php"  enctype="multipart/form-data">
         
                
                 <div class="box box-primary">
                     <div class="box-header">
                    <h4> Your Details</h4>
                     </div>
    
    <div class="col-md-12">
        <center>
            <div class="form-group">

                <label for="inputEmail3" class="  control-label">Image </label>
                <br>
                <img id="output" src="../../model/users/users_img/<?= $row['image'] ?>"  />

                <input type="file" id="file" class="form-control " onchange="loadFile(event)" name="image" >

                <script>
                    var loadFile = function (event) {
                        var output = document.getElementById('output');
                        output.src = URL.createObjectURL(event.target.files[0]);
                    };
                </script>


            </div>
        </center>     

    </div>
    <div class="row">
        <div class ="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Firstname  </label>
                <div class="col-md-8">
                    <input type="text" name="firstname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="<?= $row['firstname'] ?>" required="">
                </div>

            </div>
        </div>

        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Lastname</label>
                <div class="col-md-8">
                    <input type="text" name="lastname" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="<?= $row['lastname'] ?>">
                </div>

            </div>

        </div>

    </div>
    <div class="col-md-12"><br></div>
    <div class="row">
        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Username </label>
                <div class="col-md-8">
                    <input type="text" name="username" class="form-control" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" value="<?= $row['username'] ?>" required="">
                </div>

            </div>
        </div>


        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Usertype</label>
                <div class="col-md-8">
                    <input type="text" class="form-control"  value="<?= $row['user_type'] ?>" disabled="">
                </div>

            </div>
        </div>

    </div>


    <div class="col-md-12"><br></div> 
    
   
    <div class="row">

        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Address</label>
                <div class="col-md-8">
                    <input type="text" name="address" class="form-control" value="<?= $row['address'] ?>">
                </div>

            </div>
        </div>

        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">Mobile</label>
                <div class="col-md-8">
                    <input type="text" name="mobile" class="form-control" maxlength="10" value="<?= $row['mobile'] ?>">
                </div>

            </div>
        </div>


    </div>

    <div class="col-md-12"><br></div> 
    <div class="row">

        <div class ="col-md-6 ">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">NIC No </label>
                <div class="col-md-8">
                    <input type="text" pattern="^(?:0?[0-9]{9}[V]|[0-9]{12})$" class="form-control" name="nic" title="xxxxxxxxxxxx or xxxxxxxxxV" maxlength="12" value="<?= $row['nic'] ?>" required="">
                </div>

            </div>
        </div>

        <div class ="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-4 control-label">E-mail </label>
                <div class="col-md-8">
                    <input type="email" class="form-control" name="email" pattern="[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="<?= $row['email'] ?>">
                </div> 

            </div>
        </div>
        <div class="col-md-12"><br></div> 
        <input type="hidden" value="<?= $id ?>" name="id">
    </div>
<div class="box-footer">
  <button   class="btn btn-primary pull-right add_staf" >Edit</button>
</div>
</div>
                
        </form>
    
</div>

 <div class="col-md-4">
     <form method="post" action=""   id="my_form">
     
         
          <div class="box box-primary collapsed-box box-solid" >
              <div class="box-header with-border" >
              <h3 class="box-title">Reset Password</h3>

              <div class="box-tools pull-right">
                  <button style="width: 20px; height: 20px" type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" >
                
            
                                        <div class="form-wrapper">
                                            <label for="">Old Password</label>
                                            <input type="password" class="form-control" name="old_pass" required="">
                                        </div>
                                        <div class="form-wrapper">
                                            <label for="">New Password</label>
                                            <input type="password" class="form-control" name="new_pass1" required="">
                                        </div>
                <div class="form-wrapper">
                                            <label for="">Repeat Password</label>
                                            <input type="password" class="form-control" name="new_pass2" required="" >
                                        </div>
                                       

                                   
                
            </div>
            <div class="box-footer">
                <center> 
                        <button   class="btn btn-primary pull-right"  name="update_pass" > Reset Password</button>
            </center>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
     </form>
        </div>
<script>
$(document).on('submit', '#my_form', function(event){
		event.preventDefault();
		
		var form_data = $(this).serialize();
		$.ajax({
			url:"../../model/users/ChangePassword.php",
			method:"POST",
			data:form_data,
                         dataType: 'json',
			success:function(data)
			{
				
                                if(data.location){
                                     alert(data.alert);
                                    window.location = data.location;
                                }else{
                                    alert(data.alert);
                                }
			}
		})
	});

</script>
<?php include '../theme/footer.php'; ?>


