    <?php

$payments_active = "active";
$payments_details = "active";

include '../theme/header.php';


include '../../controle/db.php';

    

?>


<?php
$sql = "SELECT *  FROM user INNER JOIN stu_course ON user.stu_id = stu_course.stu_id
        INNER JOIN course ON stu_course.course = course.course_code && stu_course.sub_course = course.course_name
         ORDER BY stu_course.stu_id DESC  ";

$sql_old = "SELECT *  FROM user INNER JOIN old_stu_course ON user.stu_id = old_stu_course.stu_id
        INNER JOIN course ON old_stu_course.course = course.course_code && old_stu_course.sub_course = course.course_name
         ORDER BY old_stu_course.stu_id DESC  ";

$sql_d = "SELECT *  FROM user INNER JOIN payments ON user.stu_id = payments.stu_id ORDER BY payments.stu_id DESC  ";
$query_u = mysqli_query(connect(), $sql);
$query_u_old = mysqli_query(connect(), $sql_old);
$query_d = mysqli_query(connect(), $sql_d);
$total_d = mysqli_num_rows($query_d);
$total_u = mysqli_num_rows($query_u);
?> 

<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#all" data-toggle="tab"><button class="btn btn-primary " type="button" >Currunt Payments</button></a></li>
            
            <li class=""><a href="#all_old" data-toggle="tab"><button class="btn btn-primary " type="button" >Old Payments</button></a></li>

            <li ><a href="#deleted" data-toggle="tab"><button class="btn btn-danger " type="button" >all payments</button></a></li>


        </ul>


        <div class="tab-content">
            <div class="active tab-pane" id="all">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3> Student currunt payments</h3>
                        

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table  class="table table-bordered table-striped data">
                            <thead>


                                <tr>
                                    <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                    <th>Course fee</th>
                                    <th>Toal Paid</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    

                                    <th>Actions</th>
                                </tr>
                            </thead>





                            <?php
                            $total = 0;
                            while ($row_u = mysqli_fetch_array($query_u)) {
                                $id= $row_u['stu_id'];
                                $sub_cou = $row_u['sub_course'];
                                $cou = $row_u['course'];
                                $total = 0;
                                
                                
                                $sql3 = "SELECT * FROM  payments where course ='$cou' && sub_cou='$sub_cou' && stu_id='$id'";
                                 $query3 = mysqli_query(connect(), $sql3) or die;
                                 $count = mysqli_num_rows($query3);
                                 while ($row1 = mysqli_fetch_array($query3)) {
                                      $total= $total +$row1['payments'];
                                 }
                                
                                 if ( $total == $row_u['fees']  ){
                                     
                                     $btn = "success";
                                     $btn1 = "Fully Paid";
                                 } else {
                                      $btn = "warning";
                                      $btn1 = "pending";
                                 }
                                
                                ?>
                                <tr>
                                    <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                                    <td><?= $row_u['stu_id'] ?></td>
                                    <td><?= $row_u['title'] . '. ' . ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                                    <td><?= $row_u['course'] ?></td>
                                    <td><?= $row_u['sub_course'] ?></td>
                                    <td><?= $row_u['fees'] ?></td>
                                    <td> <?= $total ?></td>
                                    <td> <?= $row_u['fees'] - $total ?></td>
                                    
                                     <td>
                          <button type="button" class="btn btn-<?= $btn ?> btn-xs  "><?= $btn1 ?></button>           
                                     </td>



                                    <td>
                                        

                                        <button type="button" id="<?= $row_u['stu_id'] ?>" value="<?= $row_u['course'] ?>" name="<?= $row_u['sub_course'] ?>" class="btn btn-info btn-xs  vou_view">paid info</button>
                                        


                                    </td>

                                </tr>

                                  <?php 
                                   
                                 
                                   
                                     } ?>



                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                    <th>Course fee</th>
                                     <th>Toal Paid</th>
                                     <th>Balance</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            
                 <div class=" tab-pane" id="all_old">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3> Student Old payments</h3>
                        

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table  class="table table-bordered table-striped data">
                            <thead>


                                <tr>
                                    <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                    <th>Course fee</th>
                                    <th>Toal Paid</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    

                                    <th>Actions</th>
                                </tr>
                            </thead>





                            <?php
                            $total = 0;
                            while ($row_u_old = mysqli_fetch_array($query_u_old)) {
                                $id= $row_u_old['stu_id'];
                                $sub_cou = $row_u_old['sub_course'];
                                $cou = $row_u_old['course'];
                                $total = 0;
                                
                                
                                $sql3 = "SELECT * FROM  payments where course ='$cou' && sub_cou='$sub_cou' && stu_id='$id'";
                                 $query3 = mysqli_query(connect(), $sql3) or die;
                                 $count = mysqli_num_rows($query3);
                                 while ($row1 = mysqli_fetch_array($query3)) {
                                      $total= $total +$row1['payments'];
                                 }
                                
                                 if ( $total == $row_u_old['fees']  ){
                                     
                                     $btn = "success";
                                     $btn1 = "Fully Paid";
                                 } else {
                                      $btn = "warning";
                                      $btn1 = "pending";
                                 }
                                
                                ?>
                                <tr>
                                    <td><img src="../../model/student/Student_img/<?= $row_u_old['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                                    <td><?= $row_u_old['stu_id'] ?></td>
                                    <td><?= $row_u_old['title'] . '. ' . ucfirst($row_u_old['firstname']) . ' ' . $row_u['lastname'] ?></td>
                                    <td><?= $row_u_old['course'] ?></td>
                                    <td><?= $row_u_old['sub_course'] ?></td>
                                    <td><?= $row_u_old['fees'] ?></td>
                                    <td> <?= $total ?></td>
                                    <td> <?= $row_u_old['fees'] - $total ?></td>
                                    
                                     <td>
                          <button type="button" class="btn btn-<?= $btn ?> btn-xs  "><?= $btn1 ?></button>           
                                     </td>



                                    <td>
                                        

                                        <button type="button" id="<?= $row_u_old['stu_id'] ?>" value="<?= $row_u_old['course'] ?>" name="<?= $row_u_old['sub_course'] ?>" class="btn btn-info btn-xs  vou_old_view">paid info</button>
                                        


                                    </td>

                                </tr>

                                  <?php 
                                   
                                 
                                   
                                     } ?>



                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                    <th>Course fee</th>
                                     <th>Toal Paid</th>
                                     <th>Balance</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

            <div class="tab-pane" id="deleted">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3> Student all payments Details</h3>
                        
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table  class="table table-bordered table-striped data">
                            <thead>


                                <tr>
                                   <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                     <th>Payment</th>
                                    <th>Pay order</th>
                                    <th>Paid at</th>
                                     <th>Actions</th>
                                   
                                </tr>
                            </thead>





                            <?php
                            while ($row_u = mysqli_fetch_array($query_d)) {
                                ?>
                                <tr>
                                   <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                                    <td><?= $row_u['stu_id'] ?></td>
                                    <td><?= $row_u['title'] . '. ' . ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                                    <td><?= $row_u['course'] ?></td>
                                    <td><?= $row_u['sub_cou'] ?></td>
                                     <td><?= $row_u['payments'] ?></td>
                                      <td><?= $row_u['pay_order'] ?></td>
                                      <td>&nbsp;<?= $row_u['created_at_fee'] ?> &nbsp;</td>
                                      <td><a href="../print/payment.php?stu_newid=<?= $row_u['id']  ?>&location=../payments/payment_report.php">
                                              <button type="button" class="btn btn-xs btn-warning">Print</button></a></td>


                                   

                                </tr>

<?php } ?>



                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Image</th>
                                    <th>Id</th>
                                    <th>FullName</th>
                                    <th>Course</th>
                                    <th>Course Level</th>
                                     <th>Payment</th>
                                    <th>Pay order</th>
                                    <th>Paid at</th>
                                     <th>Actions</th>
                                   
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>

<div id="s_payment_detail" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="product_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-user"></i> More Details</h4>
                        </div>
                        <div class="modal-body" id="p_details">
                          
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
<script type="text/javascript">
    
 
       
    $(document).ready(function () {
        
         $('.data').DataTable({

        });
        
        $(document).on('click', '.vou_view', function(){
        var stu_id = $(this).attr("id");
         var  sub_cou= $(this).attr("name");
          var cou = $(this).val();
        var btn_action = 'vou_view';
        $.ajax({
            url:"../../model/ajax/payment_modal.php",
            method:"POST",
            data:{stu_id1:stu_id, btn_action:btn_action,cour:cou,sub_cour:sub_cou},
            success:function(data){
                $('#s_payment_detail').modal('show');
                $('#p_details').html(data);
            }
        })
    });
    
        $(document).on('click', '.vou_old_view', function(){
        var stu_id = $(this).attr("id");
         var  sub_cou= $(this).attr("name");
          var cou = $(this).val();
          
        //  alert(stu_id+sub_cou+cou)
        var btn_action = 'vou_old_view';
        $.ajax({
            url:"../../model/ajax/payment_modal.php",
            method:"POST",
            data:{stu_id1:stu_id, btn_action:btn_action,cour:cou,sub_cour:sub_cou},
            success:function(data){
                $('#s_payment_detail').modal('show');
                $('#p_details').html(data);
            }
        })
    });
    });</script>


<?php include '../theme/footer.php'; ?>