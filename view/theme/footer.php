
<!-- /.row -->
</section>
<!-- /.content -->
</div>


<footer class="main-footer">

    <strong>Copyright &copy; Hallam City Campus.</strong> All rights
    reserved.
</footer>


<div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->

<!-- angular -->

<!-- Bootstrap 3.3.7 -->
<script src="../../src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../src/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../src/plugins/input-mask/jquery.inputmask.js"></script>
<!-- date-range-picker -->
<script src="../../src/bower_components/moment/min/moment.min.js"></script>

<script src="../../src/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../../src/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../src/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../src/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../../src/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../src/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../src/dist/js/demo.js"></script>
<script src="../../src/bower_components/PACE/pace.min.js"></script>
<script  src="../../src/dist/js/outocomplete/jquery-ui.js"></script>


<script src="../../src/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="../../src/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function () {
        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $("#tags").autocomplete({
            source: availableTags
        });
    });
</script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'})
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        '15 days': [moment(), moment().add(15, 'days')],
                        'next month': [moment(), moment().add(1, 'month')],
                        '45 days': [moment(), moment().add(45, 'days')],
                        'next 2 month': [moment(), moment().add(2, 'month')],
                    },
                    startDate: moment(),
                    endDate: moment().add(1, 'month'),
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
        
        $("#created_at").datepicker().datepicker("setDate", new Date());
    })
</script>
<script src="../../src/dist/js/validator.min.js"></script>
<script src="../../src/bower_components/chart.js/Chart.js"></script>

<script type="text/javascript" src="../../src/dist/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

 $("#save_form").on('click', function () {
                    if (!$(this).hasClass('disabled')) {
                        var elmForm = $("#myForm");
                        if (elmForm) {
                            elmForm.validator('validate');
                            var elmErr = elmForm.find('.has-error');
                            if (elmErr && elmErr.length > 0) {
                                alert('please fill all required fields');
                                return false;
                            } else {
elmForm.attr('action','../../model/student/save.php');
                                elmForm.submit();
                                $(".finish").addClass('hide');
                                $("#save_form").addClass('hide');
                              
                                    Pace.restart()
                               
                                return false;
                            }
                        }
                    }
                });


        var btnFinish = $('<button></button>').text('Finish')
                .addClass('btn btn-primary finish hide')

                .on('click', function () {
                    if (!$(this).hasClass('disabled')) {
                        var elmForm = $("#myForm");
                        if (elmForm) {
                            elmForm.validator('validate');
                            var elmErr = elmForm.find('.has-error');
                            if (elmErr && elmErr.length > 0) {
                                alert('please fill all required fields');
                                return false;
                            } else {

                                elmForm.submit();
                                $(".finish").addClass('hide');
                                $("#save_form").addClass('hide');
                              
                                    Pace.restart()
                               
                                return false;
                            }
                        }
                    }
                });
        var btnCancel = $('<button></button>').text('Cancel')
                .addClass('btn btn-danger ')
                .on('click', function () {
                    $('#smartwizard').smartWizard("reset");
                    $('#myForm').find("input, textarea").val("");
                });

        // Step show event
        $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if (stepPosition === 'first') {
                $("#prev-btn").addClass('disabled');

            } else if (stepPosition === 'final') {
                $("#next-btn").addClass('disabled');
                $('.finish').removeClass('hide');
                $("#save_form").removeClass('hide');
            } else {
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');

            }
        });

        $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
            var selectedyear = $(".year option:selected").val();

            if (selectedyear != 0) {
                
                var elmForm = $("#form-step-" + stepNumber);
                // stepDirection === 'forward' :- this condition allows to do the form validation
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if (stepDirection === 'forward' && elmForm) {
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if (elmErr && elmErr.length > 0) {
                        // Form validation failed
                        return false;
                    }
                }
                return true;

            } else {

                alert("Please Select Admission Year First")
                return false;
            }
        });


        // Smart Wizard 1
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'arrows',
            transitionEffect: 'fade',
            showStepURLhash: false,
            toolbarSettings: {toolbarPosition: 'bottom',
                toolbarExtraButtons: [btnFinish, btnCancel]

            }
        });



    });


    $(document).ajaxStart(function () {
        Pace.restart();
    })

    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });
    $(document).ready(function() {
        
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    });
</script>

<!-- DataTables -->
<script src="../../src/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../src/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


</body>
</html>