
<?php  
session_start();


if (!isset($_SESSION["username"])){
    
     ?>
<script>
window.location="../../index.php";
</script>
<?php
}

if(time() - $_SESSION['timestamp'] > 3600) { //15 minute subtract new timestamp from the old one
     
		?>
       <script>
window.location="../../controle/logout.php";
</script>
<?php
        exit;
    } else {
        $_SESSION['timestamp'] = time(); //set new timestamp
    }
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" ng-app="crudApp">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="../../model/company/image/<?= $_SESSION["company_image"] ?>"ype="image/x-icon" >
  <title>Institute | Management System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../../src/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../src/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../src/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../../src/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../../src/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../../src/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../../src/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../../src/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
   
  <link rel="stylesheet" href="../../src/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../src/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../src/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../../src/bower_components/PACE/themes/black/pace-theme-center-atom.css">
<!-- Optional SmartWizard theme -->
    <link href="../../src/dist/css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" />
    <link href="../../src/dist/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
    <link href="../../src/dist/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

    <!-- Optional SmartWizard theme -->
     <!-- Include SmartWizard CSS -->
    <link href=".../../src/dist/css/smart_wizard.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="../../src/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <!-- outocmpletet -->
 <link rel="stylesheet" href="../../src/dist/js/outocomplete/jquery-ui.css">

  <!-- end -->
  <!-- Google Font -->
  <script src="../../src/bower_components/jquery/dist/jquery.min.js"></script>
  
  <style>
      
      
      
  </style>
</head>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="../../view/index/home.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="../../model/company/image/<?= $_SESSION["company_image"] ?>"  alt="User Image" style="width: 40px;height: 40px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?= $_SESSION["company_name"] ?></b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
       
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="../../model/users/users_img/<?= $_SESSION['image'] ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?= ucwords($_SESSION['firstname'] ." ". $_SESSION["lastname"]) ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../model/users/users_img/<?= $_SESSION['image'] ?>" class="img-circle" alt="User Image">

                <p>
                  <?= ucfirst($_SESSION['firstname']) ?> - <?= ucwords($_SESSION['user_type']) ?>
                  <small><?= $_SESSION['email'] ?></small>
                </p>
              </li>
              <!-- Menu Body -->
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                    <a href="../users/user_profile.php" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                    <a href="../../controle/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
          <?php if ($_SESSION["user_type"] != "user"){ ?>
          <li>
              <a href="../company/campus_profile.php" ><i class="fa fa-gears"></i></a>
          </li>
          <?php }  ?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../model/users/users_img/<?= $_SESSION['image'] ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= ucfirst($_SESSION['firstname'] ." ". $_SESSION["lastname"]) ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          
        </div>
          <br><br><br>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
       
            <?php
         if ($_SESSION["user_type"] == "admin" or $_SESSION["user_type"] == "SA" ){
         
?>
        <li class="<?= $Users_activ ?>">
            <a href="../users/user_table.php">
            <i class="fa fa-users nav_icon"></i> <span>Users</span>
           
          </a>
          
            </li>
         <?php } ?>
            
       
            <li class="<?= $Teachers_activ ?>">
                <a href="../lecturer/lecturer.php">
            <i class="fa fa-user-circle-o nav_icon"></i> <span>Lecturer </span>
           
          </a>
          
            </li>
         
            <li class="<?= $Student_active ?> treeview">
                <a href="#">
            <i class="fa fa-user nav_icon"></i> <span>Students</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?= $Student_form ?>" ><a href="../student/student.php"><i class="fa fa-circle-o"></i> New Registration</a></li>
              <li class="<?= $Student_details ?>" ><a href="../student/stdent_details.php"><i class="fa fa-circle-o"></i> Students Detail</a></li>
              <li class="<?= $Student_course ?>"><a href="../student/add_new_course.php"><i class="fa fa-circle-o"></i> Exist Student</a></li>
            
              </ul>
            </li>
             <?php
           if ($_SESSION["user_type"] == "admin" or $_SESSION["user_type"] == "SA"  ){
         
?>
            <li class="<?= $course_active ?>">
                <a href="../course/course.php">
            <i class="fa fa-book nav_icon"></i> <span>Course</span>
          </a>
        </li>
        <?php } ?>
            <li class="<?= $payments_active ?> treeview">
          <a href="#">
            <i class="fa fa-dollar nav_icon"></i> <span>Payments</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="<?= $payments_fee ?>"><a href="../payments/take_fee.php"><i class="fa fa-circle-o"></i> Take Fee</a></li>
              <li class="<?= $payments_details ?>"><a href="../payments/payment_report.php"><i class="fa fa-circle-o"></i> Payment Report</a></li>
               <li class="<?= $payments_pending ?>"><a href="../payments/all_pending_student.php"><i class="fa fa-circle-o"></i> Pending Payments </a></li>
            
              </ul>
            </li> 
            
          </ul>
        
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class=" content-wrapper container-fluid">
    <!-- Content Header (Page header) -->
    

    <!-- Main content -->
    <section class="content">
    
       
  