<?php 

$Student_active = "active";
$Student_details = "active";

include '../theme/header.php';
include '../../controle/db.php';?>


 <?php 
 
                 $sql = "SELECT * FROM user where status='active' ";
                 $query_u = mysqli_query(connect(), $sql) ;
                 $total_u = mysqli_num_rows($query_u);
                 
                 $sql_d = "SELECT * FROM user  where status='deactive'";
                 $query_d = mysqli_query(connect(), $sql_d) ;
                 $total_d = mysqli_num_rows($query_d);
                  
                        ?> 

 <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="<?= $_SESSION['active'] ?>"><a href="#all" data-toggle="tab"><button class="btn btn-primary " type="button" >All students </button></a></li>
                <?php
                $query_c = mysqli_query(connect(), "SELECT DISTINCT course_code, colour FROM course");
                while ($row_u = mysqli_fetch_array($query_c)){?>
                <li ><a href="#bit" data-toggle="tab"><button class="btn btn-info  cou" style="background: <?= $row_u['colour'] ?>;color: white" id="<?= $row_u['course_code'] ?>"><?= $row_u['course_code'] ?></button></a></li>
                <?php } ?>
                 <?php if ($_SESSION['user_type'] == 'SA'){ ?>
                 <li class="<?= $_SESSION['deactive'] ?>" ><a href="#deleted" data-toggle="tab"><button class="btn btn-danger " type="button" >Deleted students </button></a></li>
                 <?php } ?>
             
            </ul>
            <div class="tab-content">
              <div class="<?= $_SESSION['active'] ?> tab-pane" id="all">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title">Totaly You have <?= $total_u?>   Students . </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table  class="table table-bordered table-striped data">
                <thead>
                  
                   
                <tr>
                  <th>Image</th>
                  <th>Id</th>
              
                  <th>Fullname</th>
                  <th>NIC</th>
                  <th>Adress</th>
                  <th>Mobile</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    
                 <?php 
              
                    while ($row_u = mysqli_fetch_array($query_u)){
                        ?>
                <tr>
                  <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                  <td><?= $row_u['stu_id'] ?></td>
                  
                  <td><?= $row_u['title'].'. '. ucfirst($row_u['firstname']).' '.$row_u['lastname'] ?></td>
                  <td><?= $row_u['nic'] ?></td>
                  <td><?= $row_u['line1'].', <br>'. $row_u['line2'].', <br> '.$row_u['city'].', <br>'.$row_u['country'] ?></td>
                  <td><?= $row_u['mobile'] ?></td>
                  <td>
                      <br>
                     <button class="btn btn-info btn-xs glyphicon glyphicon-eye-open view" id="<?= $row_u['stu_id'] ?>" >more</button>
                      <a href="student_edit.php?id=<?= $row_u['stu_id'] ?>">
                      <button class="btn btn-success btn-xs glyphicon glyphicon-edit">edit</button> 
                      </a>
                     <?php if ($_SESSION["user_type"] != "user"){ ?>
                      <button class="btn btn-danger btn-xs glyphicon glyphicon-trash delete" id="<?= $row_u['stu_id'] ?>" name="deactive" >Delete</button>
                     <?php } ?>
                 <a href="../print/student_print.php?stu_newid=<?= $row_u['stu_id'] ?>&location=../student/stdent_details.php"> 
                     <button class="btn btn-warning btn-xs glyphicon glyphicon-print " type="button" >Print</button></a>
                   
                  </td>
                 
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Image</th>
                  <th>Id</th>
                  
                  <th>Fullname</th>
                  <th>NIC</th>
                  <th>Adress</th>
                   <th>Mobile</th>
                   <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="bit">
                  
                <!-- The timeline -->
               <div class="box-body table-responsive" id="course_details">
                   
            
                
            </div>
              </div>
              <!-- /.tab-pane -->
  <div class="<?= $_SESSION['deactive'] ?> tab-pane" id="deleted">
                <div class="box">
            <div class="box-header">
              <h3 class="box-title">Totaly You have <?= $total_d?> Deactive  Students . </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table  class="table table-bordered table-striped data">
                <thead>
                  
                   
                <tr>
                  <th>Image</th>
                  <th>Id</th>
          
                  <th>Fullname</th>
                  <th>NIC</th>
                  <th>Adress</th>
                  <th>Mobile</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                    
                 <?php 
              
                    while ($row_u = mysqli_fetch_array($query_d)){
                        ?>
                <tr>
                  <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                  <td><?= $row_u['stu_id'] ?></td>
                  
                  <td><?= $row_u['title'].'. '. ucfirst($row_u['firstname']).' '.$row_u['lastname'] ?></td>
                  <td><?= $row_u['nic'] ?></td>
                  <td><?= $row_u['line1'].', <br>'. $row_u['line2'].', <br> '.$row_u['city'].', <br>'.$row_u['country'] ?></td>
                  <td><?= $row_u['mobile'] ?></td>
                  <td>
                      <br>
                     <button class="btn btn-primary glyphicon btn-xs glyphicon-eye-open view" id="<?= $row_u['stu_id'] ?>" >view</button>
                      <a href="student_edit.php?id=<?= $row_u['stu_id'] ?>">
                      <button class="btn btn-info glyphicon glyphicon-edit btn-xs">edit</button> 
                      </a>
                      <button class="btn btn-success glyphicon btn-xs glyphicon-upload delete" id="<?= $row_u['stu_id'] ?>" name="active" >Active</button>
                    <button class="btn btn-success glyphicon btn-xs glyphicon-remove-circle delete_p" id="<?= $row_u['stu_id'] ?>" name="delete" >Delete</button>
                  
                   
                  </td>
                  
                </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Image</th>
                  <th>Id</th>
                
                  <th>Fullname</th>
                  <th>NIC</th>
                  <th>Adress</th>
                   <th>Mobile</th>
                   <th>Actions</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
              </div>
            
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>

<div id="student_detail" class="modal fade">
            <div class="modal-dialog">
                <form method="post" id="product_form">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-user"></i> More Details</h4>
                        </div>
                        <div class="modal-body">
                            <div id="details"></div>
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>


<script>
 $(document).ready(function(){
      $('.data').DataTable({
       
        
    });

  
  $(document).on('click', '.view', function(){
        var stu_id = $(this).attr("id");
        var btn_action = 'view';
        $.ajax({
            url:"../../model/ajax/view_stu.php",
            method:"POST",
            data:{stu_id:stu_id, btn_action:btn_action},
            success:function(data){
                $('#student_detail').modal('show');
                $('#details').html(data);
            }
        })
    });
    
     $(document).on('click', '.delete_p', function(){
        var stu_id = $(this).attr("id");
        var btn_action = 'delete';
        $.ajax({
            url:"../../model/student/delete_student.php",
            method:"POST",
            data:{stu_id:stu_id, delete:btn_action},
            success:function(data){
              
           window.location='stdent_details.php';
            }
        })
    });
    
      $(document).on('click', '.update_m', function(){
        var stu_id = $(this).attr("id");
        var btn_action = 'update';
        $.ajax({
            url:"../../model/ajax/view_stu.php",
            method:"POST",
            data:{stu_id:stu_id, btn_action:btn_action},
            success:function(data){
                $('#student_update').modal('show');
                $('#student_id').val(data);
            }
        })
    });
    
    
    $(document).on('click', '.cou', function(){
        var cou = $(this).attr("id");
        var btn_action = 'cou';
        $.ajax({
            url:"../../model/ajax/view_stu.php",
            method:"POST",
            data:{course:cou, btn_action:btn_action},
            success:function(data){
              
                $('#course_details').html(data);
            }
        })
    });
    
        $(document).on('click', '.delete', function(){
        var id = $(this).attr("id");
         var status = $(this).attr("name");
        var btn_action = 'delete';
   
        if(confirm("Are you sure you want to make action on this  Student?"))
        
        {
            $.ajax({
                url:"../../model/ajax/view_stu.php",
                method:"POST",
                data:{id:id, status: status, btn_action:btn_action},
                success:function(){
                   
                   window.location = "stdent_details.php"
                }
            });
        }
        else
        {
            return false;
        }
    });

});

</script>
    
<?php include '../theme/footer.php'; ?>