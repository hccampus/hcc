<?php

$Student_active = "active";
$Student_course = "active";
include '../theme/header.php';
include '../../controle/db.php';

 $active="";
if (isset($_SESSION['active_sub1'])){
    $active="";
} else {
     $active="active";
}

?>


<?php
$sql = "SELECT *  FROM stu_course INNER JOIN user ON user.stu_id = stu_course.stu_id where user.status='active' && stu_course.c_status='active'   ORDER BY stu_course.created_at DESC   ";
$sql_d = "SELECT *  FROM user INNER JOIN stu_course ON user.stu_id = stu_course.stu_id where user.status='active' && stu_course.c_status='deactive' ORDER BY stu_course.created_at DESC  ";
$query_u = mysqli_query(connect(), $sql);  
$query_d = mysqli_query(connect(), $sql_d);
$total_d = mysqli_num_rows($query_d);
$total_u = mysqli_num_rows($query_u);
?> 

<div class="col-md-12">
    <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="<?php echo $active. $_SESSION['active_sub1']; ?>"><a href="#all" data-toggle="tab"><button class="btn btn-primary " type="button" >All students </button></a></li>
               <?php if ($_SESSION['user_type'] == "SA") {?>
                 <li class="<?php echo $_SESSION['delete_sub1'] ?>"><a href="#deleted" data-toggle="tab"><button class="btn btn-danger " type="button" >Deleted students </button></a></li>
                 
               <?php } ?>
            </ul>
    

 <div class="tab-content">
              <div class="<?= $active. $_SESSION['active_sub1'] ?> tab-pane" id="all">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Student Course Details</h3>
          <button class="btn btn-warning  pull-right "data-toggle="modal" data-target="#add_course" type="button" >Add Students course</button>
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table  class="table table-bordered table-striped data">
                <thead>


                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        <th>Course</th>
                        <th>Course Level</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                
               
              
                
  
<?php
while ($row_u = mysqli_fetch_array($query_u)) {
    ?>
                        <tr>
                            <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                            <td><?= $row_u['stu_id'] ?></td>
                            <td><?= $row_u['title'] . '. ' . ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                            <td><?= $row_u['course'] ?></td>
                            <td><?= $row_u['sub_course'] ?></td>


                            <td>
                                <br>
                                
                                <button class="btn btn-primary btn-xs  update_m" id="<?= $row_u['stu_id'] ?>" name="<?= $row_u['course'] ?>" >edit Course</button>
                                <button class="btn btn-danger btn-xs  delete_sub" id="<?= $row_u['stu_id'] ?>" name="<?= $row_u['course'] ?>" value="deactive">delete</button>


                            </td>

                        </tr>
                  
<?php } ?>
               
              
                            
                </tbody>
                <tfoot>
                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        <th>Course</th>
                        <th>Course Level</th>

                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 
   
      <div class="<?= $_SESSION['delete_sub1'] ?> tab-pane" id="deleted">
    <div class="box box-primary">
        <div class="box-header">
          <h3> Student Course Details</h3>
          
       
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <table  class="table table-bordered table-striped data">
                <thead>


                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        <th>Course</th>
                        <th>Course Level</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                
               
              
                
  
<?php
while ($row_u = mysqli_fetch_array($query_d)) {
    ?>
                        <tr>
                            <td><img src="../../model/student/Student_img/<?= $row_u['image'] ?>" style="height: 50px; height: 50px; border-radius: 10px;"></td>
                            <td><?= $row_u['stu_id'] ?></td>
                            <td><?= $row_u['title'] . '. ' . ucfirst($row_u['firstname']) . ' ' . $row_u['lastname'] ?></td>
                            <td><?= $row_u['course'] ?></td>
                            <td><?= $row_u['sub_course'] ?></td>


                            <td>
                                <br>
                                
                                <button class="btn btn-primary btn-xs  update_m" id="<?= $row_u['stu_id'] ?>" name="<?= $row_u['course'] ?>" >edit Course</button>
                                <button class="btn btn-success btn-xs  delete_sub" id="<?= $row_u['stu_id'] ?>" name="<?= $row_u['course'] ?>" value="active">Active</button>
 <button class="btn btn-danger btn-xs  delete_p" id="<?= $row_u['stu_id'] ?>"  value="active">Delete</button>


                            </td>

                        </tr>
                  
<?php } ?>
               
              
                            
                </tbody>
                <tfoot>
                    <tr>
                        <th>Image</th>
                        <th>Id</th>
                        <th>FullName</th>
                        <th>Course</th>
                        <th>Course Level</th>

                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
              </div>
 </div>
    </div>
</div>
<!-- /.tab-pane -->

<!-- /.tab-pane -->

<!-- /.tab-pane -->


<div id="add_course" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="course_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i>Add new course  </h4>
                    <div id='print' style="color: red"></div>
                </div>
                 <div class="box-body">

                           <div class="form-group col-md-6">

                                <label>Your Id is </label>
                                
                                    <select   class="form-control select2 add_id "   style="width: 100%;" name="exist_id" required="" id='new_id'>
                                        <option hidden="" value="" selected="selected">Student Id</option>
                                    <?php
$sql = "SELECT stu_id FROM `user` where status='active'";
$query = mysqli_query(connect(), $sql) or die;
while ($row = mysqli_fetch_array($query)) {

    $name = $row["stu_id"];
    ?> 
                                    <option value="<?= $name ?>"><?php echo $name ?></option>
                                    <?php } ?>
                                    </select>

                               




                            </div>
                            

                           

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $("select.course_m1").change(function () {
                                        var selectedcourse = $(".course_m1 option:selected").val();
                                        $.ajax({
                                            type: "POST",
                                            url: "../../model/ajax/process-request.php",
                                            data: {course: selectedcourse}
                                        }).done(function (data) {
                                            $(".sub").html(data);
                                            $('.fees').val("");
                                        });
                                    });
                                    $("select.sub").change(function () {
                                        var selectedsubcourse = $(".sub option:selected").val();
                                         var selectedcourse = $(".course_m1 option:selected").val();
                                        $.ajax({
                                            type: "POST",
                                            url: "../../model/ajax/process-request.php",
                                            data: {sub: selectedsubcourse,course_m : selectedcourse}
                                        }).done(function (data) {
                                            
                                            $(".fees").val(data);
                                        });
                                    });
                                     
                                });</script>

                            <div class="form-group col-md-6">

                                <label>Course &Star;</label>
                                <select   class="form-control select2 course_m1"  id="course_m1 " style="width: 100%;" name="course" required="">
                                    <option hidden="" value="" selected="selected">Course</option>
<?php
$sql = "SELECT DISTINCT course_code FROM `course` ";
$query = mysqli_query(connect(), $sql) or die;
while ($row = mysqli_fetch_array($query)) {

    $name = $row["course_code"];
    ?> 
                                        <option value="<?= $name ?>"><?php echo $name ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>



                            <div class="form-group col-md-6">

                                <label>Course Level &Star;</label>
                                <select class="form-control select2 sub" id="sub" style="width: 100%;" name="sub_course" >
                                    <option disabled="" selected="selected">Level</option>




                                </select>

                            </div>
                            <div class="form-group col-md-6">

                                <label>Course Fees &Star;</label>
                                <input type="number" name='fees'  class="form-control fees">

                            </div>

                        </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-primary pull-left add_cou" >Add</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="student_update" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="Form_up">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-user"></i> Update Course level</h4>
                </div>
                <div class="modal-body">
                    <div  class="box box-primary">
                        <div class="box-body">

                            <div id="course_edit"></div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"  class="btn btn-primary pull-left cou_update">Update</button>
                    <button type="button" class="btn btn-default pull-leftl" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php if (isset($_GET['ex_id'])){      $id = $_GET["ex_id"]; $cou = $_GET["cou"]; ?> 
    <script>
        function alert1(){
    var id = "<?= $id ?>" ;
    var cou = "<?= $cou ?>";
     var btn_action = 'update';
     $.ajax({
                url: "../../model/ajax/view_stu.php",
                method: "POST",
                data: {stu_id: id, btn_action: btn_action,course: cou},
                success: function (data) {
                    $('#student_update').modal('show');
                    $('#course_edit').html(data);
                   
                }
            })
   
    }
    alert1();
    </script>
    
    <?php } ?>
    
    <?php if (isset($_GET['id_n'])){      $id = $_GET["id_n"];?> 
     <script>
        $(document).ready(function alert1(){
             
    var id = "<?= $id ?>" ;
     $('#new_id').val(id);
    $('#add_course').modal('show');
    
   
   
    });
    alert1();
    </script>
    <?php } ?>
<script>
    $(document).ready(function () {
        $('.data').DataTable({

        });
        
        $(document).on('change', '.course_m2', function () {
            var stu_id = $(this).attr("id");
            var stu_cou = $(this).attr("name");
            var btn_action = 'update';
            $.ajax({
                url: "../../model/ajax/view_stu.php",
                method: "POST",
                data: {stu_id: stu_id, btn_action: btn_action,course: stu_cou},
                success: function (data) {
                    $('#student_update').modal('show');
                    $('#course_edit').html(data);
                   
                }
            })
        });
            

  $(document).on('change', '.sub1', function () {
          // alert($(this).val())
        });


  $(document).on('click', '.update_m', function () {
            var stu_id = $(this).attr("id");
            var stu_cou = $(this).attr("name");
            var btn_action = 'update';
            $.ajax({
                url: "../../model/ajax/view_stu.php",
                method: "POST",
                data: {stu_id: stu_id, btn_action: btn_action,course: stu_cou},
                success: function (data) {
                    $('#student_update').modal('show');
                    $('#course_edit').html(data);
                   
                }
            })
        });
        $(document).on('click', '.delete_sub', function () {
            var status = $(this).val();
            var id = $(this).attr("id");
            var course = $(this).attr("name");
            var btn_action = 'delete_sub';

            if (confirm("Are you sure you want to make action on this  Student?"))

          
                $.ajax({
                    url: "../../model/ajax/view_stu.php",
                    method: "POST",
                    data: {id: id, c_status: status, btn_action: btn_action,course: course},
                    success: function () {
                        
                
                   // window.location = "add_new_course.php"
                       window.location = "add_new_course.php"
                    }
                });
           
            
        });
        
            
         $(document).on('click', '.delete_p', function () {
         
            var id = $(this).attr("id");
        
            var btn_action = 'delete';

            if (confirm("Are you sure you want Permenantly Delete this  Student Course?"))

          
                $.ajax({
                    url: "../../model/student/stu_cou_delete.php",
                    method: "POST",
                    data: {id: id,  delete: btn_action},
                    success: function () {
                        
                
                       window.location = "add_new_course.php"
                    }
                });
           
            
        });

    
    $(document).on('click', '.cou_update', function () {

        var btn_action = 'cou_update';
        var stu_id = $('#student_id').val();
        var course = $('.course_m2').val();
        var city = $('.sub1').val();
         var fees = $('.fees1').val();
        // alert(fees);
        if( city ==""){
            alert("Please select Stu id ")
        }else if(fees == ""){
             alert("Please Fill ourse fees ")
        }else{
        //var form_data = $(this).serialize();
        $.ajax({
            url: "../../model/ajax/view_stu.php",
            method: "POST",
            data: {stu_id: stu_id, course: course, sub_course: city, btn_action: btn_action,fees: fees},
            dataType:"json",
            success: function (data)
            {
                
                $('#student_update').modal('hide');
                if(confirm("Do you want make payment")){
                window.location = "../payments/take_fee.php?id="+data.stu_id+"&course="+data.stu_course+"&sub_course="+data.stu_sub_cou+"&fees="+data.stu_fees;
            }else{
                 window.location = "add_new_course.php"
            }
                
               
            }
        })
    }
        });
        $(document).on('click', '.add_cou', function () {

        var btn_action = 'add_cou';
        var id = $('.add_id').val();
        var course = $('.course_m1').val();
        var city = $('.sub').val();
        var fees = $('.fees').val();
        
        if(id ==""){
            alert("Please select Stu id ")
        }else if(course == ""){
            alert("Plese Choose couse title")
        }else if(city == ""){
            alert("Plese Choose Sub course")
        }else if(fees == ""){
            alert("Plese fill course fees")
        }else{
        //var form_data = $(this).serialize();
        $.ajax({
            url: "../../model/ajax/view_stu.php",
            method: "POST",
            data: {stu_id: id, course: course, sub_course: city, btn_action: btn_action,fees: fees},
            dataType:"json",
            success: function (data)
            {
                if(data.error == 0){
                $('#add_course').modal('hide');
                if(confirm("Do you want make payment")){
                window.location = "../payments/take_fee.php?id="+data.stu_id+"&course="+data.stu_course+"&sub_course="+data.stu_sub_cou+"&fees="+data.stu_fees;
            }else{
                 window.location = "add_new_course.php"
            }
        }else{
            $("#print").html(data.error);
        }
                
               
            }
        });
    }
        });
    });
</script>

<?php include '../theme/footer.php'; ?>