<?php

$course_active = "active";

include '../theme/header.php';
if ($_SESSION["user_type"] !== "admin" ){
    ?>
<script>
window.location = "../../index.php"
</script>

<?php
    }
?>
<html ng-app="crudApp">


<!-- Include Bootstrap CSS -->

<!-- Include main CSS -->

<!-- Include jQuery library -->
<script src="js/jQuery/jquery.min.js"></script>
<!-- Include AngularJS library -->
<script src="lib/angular/angular.min.js"></script>
<!-- Include Bootstrap Javascript -->


  
                                
<div class="box box-primary" ng-controller="DbController">
<h1 class="text-center">Add Course</h1>
<nav class="navbar navbar-default">
<div class="navbar-header">
<div class="alert alert-default navbar-brand search-box">
<button class="btn btn-primary" ng-show="show_form" ng-click="formToggle()">Add Course <span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
</div>
<div class="alert alert-default input-group search-box">
<span class="input-group-btn">
<input type="text" class="form-control" placeholder="Search .." ng-model="search_query">
</span>
</div>
</div>
</nav>
<div class="col-md-6 col-md-offset-3">

<!-- Include form template which is used to insert data into database -->
<div ng-include src="'templates/form.html'"></div>

<!-- Include form template which is used to edit and update data into database -->
<div ng-include src="'templates/editForm.html'"></div>
</div>
<div class="clearfix"></div>

<!-- Table to show employee detalis -->
<div class="box-body">
<div class="table-responsive">
<table class="table table-bordered table-striped data"  >
<tr>
    <th></th>
<th>Main Course</th>
<th>Subcourse</th>
<th>Total Course Fees</th>
<th></th>
<th></th>
</tr>
<tr ng-repeat="detail in details| filter:search_query">
<td>
<span>{{detail.id}}</span></td>
<td>{{detail.course_code}}</td>
<td>{{detail.course_name}}</td>
<td>{{detail.course_fees}}</td>
<td>
<button class="btn btn-primary" ng-click="editInfo(detail)" title="Edit"><span class="glyphicon glyphicon-edit"></span></button>
</td>
<td>
<button class="btn btn-danger" ng-click="deleteInfo(detail)" title="Delete"><span class="glyphicon glyphicon-trash"></span></button>
</td>

</tr>
</table>
</div>
</div>
</div>
                            
                       
<!-- Include controller -->
<script src="js/angular-script.js"></script>



           <?php
include '../theme/footer.php';

?>

