<?php
include 'controle/db.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" ng-app="crudApp">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="model/company/image/logo (3).png?>"ype="image/x-icon" >
  <title>Institute | Management System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="src/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="src/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="src/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="src/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="src/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="src/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="src/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="src/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
   
  <link rel="stylesheet" href="src/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="src/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="src/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="src/bower_components/PACE/themes/black/pace-theme-center-atom.css">
<!-- Optional SmartWizard theme -->
    <link href="src/dist/css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" />
    <link href="src/dist/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />
    <link href="src/dist/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />

    <!-- Optional SmartWizard theme -->
     <!-- Include SmartWizard CSS -->
    <link href=".src/dist/css/smart_wizard.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="src/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <!-- outocmpletet -->
 <link rel="stylesheet" href="src/dist/js/outocomplete/jquery-ui.css">

  <!-- end -->
  <!-- Google Font -->
  <script src="src/bower_components/jquery/dist/jquery.min.js"></script>
  
  <style>
      
      
      
  </style>
</head>
</head>
<body class="wrapper">

    <style>
    #output{

        width: 200px;

        height: 200px;
        background: #007fff;
        border-radius: 20px;

    }



    input#file {
        opacity: 0.0000001;

        width: 100px;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden;

        background: #007fff;
        border-radius: 1px;
        background-size: 1px 1px;

        margin-bottom: -40px; 
        margin-top: -120px; 
        margin-left: 40px;
    }
    input#file:hover {


        opacity: 0.99;

        width:200px!important;
        padding: 120px 0 0 0;
        height: 100px;
        overflow: hidden!important;

        background: url('src/bower_components/Ionicons/png/512/ios7-camera-outline.png') center center no-repeat;
        border-radius: 10px;
        background-size: 100px 100px;



        margin-left: 0px;
        margin-right: 0px;

    }

</style>

<br>
<br>
<!-- general form elements -->
<form action="model/student/M_student.php" id="myForm" role="form" data-toggle="validator" method="post"  enctype="multipart/form-data">
    <div id="smartwizard" style="background: white">

        <ul >
            <li><a href="#step-1">Step 1<br /><small>Select ID  </small></a></li>
            <li><a href="#step-2">Step 2<br /><small>Personal Information</small></a></li>
            <li><a href="#step-3">Step 3<br /><small>OL Results</small></a></li>
            <li><a href="#step-4">Step 4<br /><small>AL results</small></a></li>
            <li><a href="#step-5">Step 5<br /><small>Emergency Details</small></a></li>
            <br>
            
           
            <a href="index.php"> <button class="btn btn-warning pull-right " type="button" id="" >Back</button></a>
            
        
        </ul>





        <div>




            <div id="step-1" class="form-group" >

                <div id="form-step-0" role="form" data-toggle="validator">


                    <div class="form-group col-md-4">


                        <br><br>

                        <select id="year_ad" name="year_ad" class="btn btn-info dropdown-toggle year select2"  required="" >
                            <option hidden="" value="0" >Admission Year</option>
                            <?php
                            foreach (range(2012, (int) date("Y")) as $year) {
                                echo '<option value="' . $year . '"' . ($year == $_GET["year"] ? ' selected="selected"' : '') . ' >' . $year . '</option>';
                            }
                            ?>

                        </select>
                        <div class="help-block with-errors"></div>
                        <br><br>
                       

                    </div>
                    <input type="hidden" name="stu_id" value="new application">
      
                    <div class="form-group col-md-4">
                        <label>Student image &Star;</label>
                        <br>
                        <img id="output"   >
                        <input id="file" onchange="loadFile(event)" type="file" name='image' >

                        <script>
                            var loadFile = function (event) {
                                var output = document.getElementById('output');
                                output.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                    </div>


                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("select.year").change(function () {
                                $(".stu").val("");
                            });
                            $("#auto_gen").click(function () {
                                var selectedyear = $(".year option:selected").val();
                                if (selectedyear != 0) {
                                    $.ajax({
                                        type: "POST",
                                        url: "model/ajax/process-request.php",
                                        data: {year: selectedyear}
                                    }).done(function (data) {
                                        $(".stu").val(data);
                                        $(".stu").html(data);
                                    });
                                } else {
                                    alert("Please Select Admission Year First")
                                }

                            });
                        });</script>




                </div>

            </div>

            <div id="step-2" class="form-group " >
                <div id="form-step-1" role="form" data-toggle="validator" >


                    <div class="form-group col-md-3">
                        <label>Title</label>
                        <div>
                            <div class="radio-inline">
                                <input type="radio" name="title" class="minimal"
                                       value="mr" <?= isset($_REQUEST["title"]) && $_REQUEST["title"] == "mr" ? "checked" : ""; ?> >Mr
                            </div>
                            <div class="radio-inline">
                                <input type="radio" name="title" class="minimal"
                                       value="ms" <?= isset($_REQUEST["title"]) && $_REQUEST["title"] == "ms" ? "checked" : ""; ?> >Miss
                            </div>
                            <div class="radio-inline">					
                                <input type="radio" name="title" class="minimal"
                                       value="mrs." <?= isset($_REQUEST["title"]) && $_REQUEST["title"] == "mrs." ? "checked" : ""; ?> >Mrs.
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label>First Name &Star;</label>
                        <input type="text"  name="firstname" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control" title="letters Only"
                               value="<?= isset($_REQUEST["firstname"]) ? $_REQUEST["firstname"] : ''; ?>"
                               placeholder="First Name" required="">

                    </div>


                    <div class="form-group col-md-3">
                        <label>Last Name</label>
                        <input type="text"  name="lastname" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" id='lastname' class="form-control" title="letter only"
                               value="<?= isset($_REQUEST["lastname"]) ? $_REQUEST["lastname"] : ''; ?>"
                               placeholder="Last Name" >
                    </div> 


                    <div class="form-group col-md-3">
                        <label>NIC &Star;</label>
                        <input type="text" pattern="^(?:0?[0-9]{9}[V]|[0-9]{12})$" name="nic" id="nic" title="xxxxxxxxxxxx or xxxxxxxxxV" class="form-control"
                               placeholder="NIC NO" maxlength="12"
                               value="<?= isset($_REQUEST["nic"]) ? $_REQUEST["nic"] : ''; ?>"  required="">
                    </div>




                    <div class="form-group col-md-3">
                        <label>Gender &Star;</label>
                        <div>
                            <div class="radio-inline">
                                <input type="radio" name="gender" class="minimal"
                                       value="male" <?= isset($_REQUEST["gender"]) && $_REQUEST["gender"] == "male" ? "checked" : ""; ?> required="">Male
                            </div>
                            <div class="radio-inline">
                                <input type="radio" name="gender" class="minimal"
                                       value="female" <?= isset($_REQUEST["gender"]) && $_REQUEST["gender"] == "female" ? "checked" : ""; ?> >Female
                            </div>
                        </div>
                    </div>


                    <div class="form-group col-md-3">
                        <label>Civil Status</label>
                        <div>
                            <div class="radio-inline">
                                <input type="radio" name="civilstatus" class="minimal"
                                       value="single" <?= isset($_REQUEST["civilstatus"]) && $_REQUEST["civilstatus"] == "single" ? "checked" : ""; ?>>Single

                            </div>
                            <div class="radio-inline">
                                <input type="radio" name="civilstatus" class="minimal"
                                       value="maried" <?= isset($_REQUEST["civilstatus"]) && $_REQUEST["civilstatus"] == "maried" ? "checked" : ""; ?>>Maried
                            </div>
                        </div>
                    </div>



                    <div class="form-group col-md-3">
                        <label>Date of Birth &Star;</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="dateofbirth" class="form-control" id="datepicker"

                                   value="<?= isset($_REQUEST["dateofbirth"]) ? $_REQUEST["dateofbirth"] : ''; ?>" required="">
                        </div>
                    </div>

                    <script>
                        $(document).ready(function () {
                            $("#datepicker").change(function () {
                                var Datepicker = ($(this).val()).substr(6);
                                var d = new Date();

                                var month = d.getMonth() + 1;
                                var day = d.getDate();

                                var output = (('' + day).length < 2 ? '0' : '') + day + '/' +
                                        (('' + month).length < 2 ? '0' : '') + month + '/' + d.getFullYear()
                                        ;
                                output = output.substr(6);

                                
                                if(output-6 <= Datepicker){
                                    
                                    alert("Sorry this date is not Valid")
                                    $(this).val("")
                                }


                            })

                        })
                    </script>

                    <div class="form-group col-md-3">
                        <label>Line1 &Star;</label>
                        <input type="text" class="form-control" name="line1"
                               placeholder="addres"
                               value="<?= isset($_REQUEST["line1"]) ? $_REQUEST["line1"] : ''; ?>" required=""> 
                    </div>






                    <div class="form-group col-md-3">
                        <label>Line2 </label>
                        <input type="text" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control" name="line2"
                               placeholder="addres"
                               value="<?= isset($_REQUEST["line2"]) ? $_REQUEST["line2"] : ''; ?>" > 
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("select.country").change(function () {
                                var selectedCountry = $(".country option:selected").val();
                                $.ajax({
                                    type: "POST",
                                    url: "model/ajax/process-request.php",
                                    data: {country: selectedCountry}
                                }).done(function (data) {
                                    $(".country1").html(data);
                                });
                            });
                        });</script>

                    <div class="form-group col-md-3">

                        <label>Country &Star;</label>
                        <select class="form-control select2 country"  id="country " style="width: 100%;" name="country">
                            <option disabled="" selected="selected">Country</option>
                            <?php
                            $sql = "SELECT * FROM `countries` ";
                            $query = mysqli_query(connect(), $sql) or die;
                            while ($row = mysqli_fetch_array($query)) {
                                $id = $row['id'];
                                $name = $row['name'];
                                ?> 
                                <option value="<?= $name ?>"><?php echo $name ?></option>
                            <?php } ?>
                        </select>

                    </div>



                    <div class="form-group col-md-3">

                        <label>City &Star;</label>
                        <select class="form-control select2 country1" id="city" style="width: 100%;" name="city" >
                            <option disabled="" selected="selected">City</option>




                        </select>

                    </div>
                    <div class="form-group col-md-3">
                        <label>Mobile &Star;</label>
                        <input type="text" pattern="[0-9]{10}" maxlength="10" name="mobile"  title="Phone numbers only" class="form-control" 
                               placeholder=" Mobile NO"
                               value="<?= isset($_REQUEST["mobile"]) ? $_REQUEST["mobile"] : ''; ?>"  required="">
                    </div>





                    <div class="form-group col-md-3">
                        <label>Work</label>
                        <input type="text" pattern="[0-9]{10}" maxlength="10" name="work" title="Phone numbers only" class="form-control"
                               placeholder="Work Mobile NO"
                               value="<?= isset($_REQUEST["work"]) ? $_REQUEST["work"] : ''; ?>" >
                    </div>




                    <div class="form-group col-md-6">
                        <label>Email</label>
                        <input type="email" pattern="[a-z0-9.+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" name="email" class="form-control"
                               placeholder="Email Address"
                               value="<?= isset($_REQUEST["email"]) ? $_REQUEST["email"] : ''; ?>" >
                    </div>


                </div>  
            </div>
            <!--  end form 1  -->
            <div id="step-3" class="form-group">
                <div id="form-step-2" role="form" data-toggle="validator">
                    <div class="form-group col-md-3 col-md-offset-9">

                        <label>Year : </label>
                        <select id="year_ol" name="year_ol" class="form-control-static"   >
                            <option hidden>not sit</option>
                            <?php
                            foreach (range(2005, (int) date("Y")) as $year) {


                                echo '<option value="' . $year . '"' . ($year == $_GET["year"] ? ' selected="selected"' : '') . ' >' . $year . '</option>';
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"  value="Religion" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="religion"  >
                                    <option value="not selected" hidden>grade</option> 
                                    <option  value="A" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["religion"]) && $_REQUEST["religion"] == "ab") ? "selected" : ""; ?>>ab</option>



                                </select> 
                            </div>
                        </div>
                        <br>
                    </div>

                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"  value="Maths" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="maths" >

                                    <option value="not selected" hidden>grade</option> 
                                    <option  value="A" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["maths"]) && $_REQUEST["maths"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>  
                        <br>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"  value="Science" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="science" >

                                    <option value="not selected" hidden>grade</option> 
                                    <option  value="A" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["science"]) && $_REQUEST["science"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>   
                    </div>   
                    <div class="col-md-12"> <br></div>
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"   value="Language" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="tamil" >

                                    <option value="not selected" hidden>grade</option>
                                    <option  value="A" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["tamil"]) && $_REQUEST["tamil"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div> 
                        <br>
                    </div>                        
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"   value="History" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="history" >

                                    <option value="not selected" hidden>grade</option>
                                    <option  value="A" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["history"]) && $_REQUEST["history"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>  
                        <br>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" class="form-control"   value="English" readonly="">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="english" >

                                    <option value="not selected" hidden>grade</option> 
                                    <option  value="A" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["english"]) && $_REQUEST["english"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>   
                    </div>     
                    <div class="col-md-12"> <br></div>
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" pattern="[a-z]{0-10}" class="form-control"  maxlength="10" name="ol_subject7" value="<?= isset($_REQUEST["ol_subject7"]) ? $_REQUEST["ol_subject7"] : ''; ?>" >
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="ol_grade7" >

                                    <option value="not selected" hidden>grade</option>
                                    <option  value="A" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["ol_grade7"]) && $_REQUEST["ol_grade7"] == "ab") ? "selected" : ""; ?>>ab</option>
                                </select> 
                            </div>
                        </div>  
                        <br>
                    </div>                          
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control"   name="ol_subject8" value="<?= isset($_REQUEST["ol_subject8"]) ? $_REQUEST["ol_subject8"] : ''; ?>">
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="ol_grade8" >

                                    <option value="not selected" hidden>grade</option>
                                    <option  value="A" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["ol_grade8"]) && $_REQUEST["ol_grade8"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>   
                        <br>
                    </div>                          
                    <div class="form-group col-md-4">
                        <div class="input-group input-group-md">
                            <input type="text"  pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control"  name="ol_subject9" value="<?= isset($_REQUEST["ol_subject9"]) ? $_REQUEST["ol_subject9"] : ''; ?>" >
                            <div class="input-group-btn">

                                <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="ol_grade9" >

                                    <option value="not selected" hidden>grade</option>
                                    <option  value="A" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "A") ? "selected" : ""; ?>>A</option>
                                    <option  value ="B" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "B") ? "selected" : ""; ?>>B</option>
                                    <option  value="C" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "C") ? "selected" : ""; ?>>C</option>
                                    <option  value="S" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "S") ? "selected" : ""; ?>>S</option>
                                    <option  value="F" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "F") ? "selected" : ""; ?>>F</option>
                                    <option   value="ab" <?= (isset($_REQUEST["ol_grade9"]) && $_REQUEST["ol_grade9"] == "ab") ? "selected" : ""; ?>>ab</option>

                                </select> 
                            </div>
                        </div>   
                    </div> 
                    <div class="col-md-12"> <br></div>
                </div>
            </div>
            <!--  end form 2  -->      
            <div id="step-4" class="form-group">
                <div id="form-step-3" role="form" data-toggle="validator">
                    <div class="form-group col-md-12 ">

                        <div class="pull-right">
                            <label>Stream : </label>
                            <input type="text" class="form-control-static"  name="stream"  style="width: 50%">

                        </div>
                        <div class="pull-left">
                            <label>Year : </label>
                            <select id="year_al" name="year_al" class="form-control-static"   >
                                <option hidden>not sit</option>
                                <?php
                                foreach (range(2005, (int) date("Y")) as $year) {


                                    echo '<option value="' . $year . '"' . ($year == $_GET["year"] ? ' selected="selected"' : '') . ' >' . $year . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12 ">
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="input-group input-group-md">
                                    <input type="text"  class="form-control" maxlength="10" name="al_subject1" value="<?= isset($_REQUEST["al_subject1"]) ? $_REQUEST["al_subject1"] : ''; ?>">
                                    <div class="input-group-btn">

                                        <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="al_grade1" >

                                            <option value="not selected" hidden>grade</option>
                                            <option  value="A" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "A") ? "selected" : ""; ?>>A</option>
                                            <option  value ="B" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "B") ? "selected" : ""; ?>>B</option>
                                            <option  value="C" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "C") ? "selected" : ""; ?>>C</option>
                                            <option  value="S" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "S") ? "selected" : ""; ?>>S</option>
                                            <option  value="F" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "F") ? "selected" : ""; ?>>F</option>
                                            <option   value="ab" <?= (isset($_REQUEST["al_grade1"]) && $_REQUEST["al_grade1"] == "ab") ? "selected" : ""; ?>>ab</option>

                                        </select> 
                                    </div>
                                </div>   
                                <br>
                            </div>                                         

                            <div class="col-md-6 ">
                                <div class="input-group input-group-md">
                                    <input type="text"  pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$"  class="form-control"  name="al_subject2" value="<?= isset($_REQUEST["al_subject2"]) ? $_REQUEST["al_subject2"] : ''; ?>">
                                    <div class="input-group-btn">

                                        <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="al_grade2" >

                                            <option value="not selected" hidden>grade</option> 
                                            <option  value="A" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "A") ? "selected" : ""; ?>>A</option>
                                            <option  value ="B" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "B") ? "selected" : ""; ?>>B</option>
                                            <option  value="C" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "C") ? "selected" : ""; ?>>C</option>
                                            <option  value="S" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "S") ? "selected" : ""; ?>>S</option>
                                            <option  value="F" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "F") ? "selected" : ""; ?>>F</option>
                                            <option   value="ab" <?= (isset($_REQUEST["al_grade2"]) && $_REQUEST["al_grade2"] == "ab") ? "selected" : ""; ?>>ab</option>

                                        </select> 
                                    </div>
                                </div>   
                            </div>      
                            <div class="col-md-12"><br></div>
                            <div class="col-md-6 ">
                                <div class="input-group input-group-md">
                                    <input type="text"   pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control"   name="al_subject3" value="<?= isset($_REQUEST["al_subject3"]) ? $_REQUEST["al_subject3"] : ''; ?>">
                                    <div class="input-group-btn">

                                        <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="al_grade3" >

                                            <option  value="not selected" hidden>grade</option>
                                            <option  value="A" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "A") ? "selected" : ""; ?>>A</option>
                                            <option  value ="B" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "B") ? "selected" : ""; ?>>B</option>
                                            <option  value="C" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "C") ? "selected" : ""; ?>>C</option>
                                            <option  value="S" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "S") ? "selected" : ""; ?>>S</option>
                                            <option  value="F" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "F") ? "selected" : ""; ?>>F</option>
                                            <option  value="ab" <?= (isset($_REQUEST["al_grade3"]) && $_REQUEST["al_grade3"] == "ab") ? "selected" : ""; ?>>ab</option>

                                        </select> 
                                    </div>
                                </div>   
                            </div> 
                            <br>
                            <div class="col-md-6 ">
                                <div class="input-group input-group-md">
                                    <input type="text"  pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" class="form-control" name="al_subject4" value="General English" value="<?= isset($_REQUEST["al_subject4"]) ? $_REQUEST["al_subject4"] : ''; ?>" readonly="">
                                    <div class="input-group-btn">

                                        <select class="btn btn-primary dropdown-toggle" data-toggle="dropdown" class="form-control-static" name="al_grade4" >

                                            <option value="not selected" hidden>grade</option>  
                                            <option  value="A" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "A") ? "selected" : ""; ?>>A</option>
                                            <option  value ="B" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "B") ? "selected" : ""; ?>>B</option>
                                            <option  value="C" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "C") ? "selected" : ""; ?>>C</option>
                                            <option  value="S" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "S") ? "selected" : ""; ?>>S</option>
                                            <option  value="F" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "F") ? "selected" : ""; ?>>F</option>
                                            <option  value="ab" <?= (isset($_REQUEST["al_grade4"]) && $_REQUEST["al_grade4"] == "ab") ? "selected" : ""; ?>>ab</option>                             

                                        </select> 
                                    </div>
                                </div>   
                            </div>



                        </div>
                    </div>     
                </div>
            </div>
            <!--  end form 3  -->
            <div id="step-5" class="form-group">
                <div id="form-step-4" role="form" data-toggle="validator">
                    <div class="form-group col-md-4">
                        <label>Gurdian Name &Star;</label>
                        <input type="text" class="form-control" name="p_name" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$" id='g_name'
                               value="<?= isset($_REQUEST["Name"]) ? $_REQUEST["Name"] : ''; ?>" placeholder="gurdian name"
                               required="">

                    </div>


                    <div class="form-group col-md-4">
                        <label>Relationship</label>
                        <input type="text" class="form-control" name="relationship" pattern="^[a-zA-Z]+\s?[a-zA-Z]+*$"
                               value="Father"
                               >
                    </div> 

                    <div class ="col-md-12 "> <span><h4><b>Contact No</b></h4></span></div>

                    <div class="form-group col-md-3">
                        <label>Mobile1 &Star;</label>
                        <input type="text" class="form-control" name="mobile_1"  pattern="[0-9]{10}" maxlength="10"
                               value="<?= isset($_REQUEST["mobile_1"]) ? $_REQUEST["mobile_1"] : ''; ?>"
                               required="">
                    </div>

                    <div class="form-group col-md-3">
                        <label>Mobile2</label>
                        <input type="text" class="form-control" name="mobile_2"  pattern="[0-9]{10}" maxlength="10"
                               value="<?= isset($_REQUEST["mobile_2"]) ? $_REQUEST["mobile_2"] : ''; ?>"
                               >
                    </div>

                    <div class="form-group col-md-3">

                        <label>Mobile3</label>
                        <input type="text" class="form-control" name="mobile_3"  pattern="[0-9]{10}" maxlength="10"
                               value="<?= isset($_REQUEST["mobile_3"]) ? $_REQUEST["mobile_3"] : ''; ?>" >


                    </div>
                </div>
            </div>
            <!--  end form 4  -->

        </div>
    </div>
</form>
   
<!-- ./wrapper -->
<!-- jQuery 3 -->

<!-- angular -->

<!-- Bootstrap 3.3.7 -->
<script src="src/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="src/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="src/plugins/input-mask/jquery.inputmask.js"></script>
<!-- date-range-picker -->
<script src="src/bower_components/moment/min/moment.min.js"></script>

<script src="src/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="src/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="src/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="src/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="src/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="src/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="src/dist/js/demo.js"></script>
<script src="src/bower_components/PACE/pace.min.js"></script>
<script  src="src/dist/js/outocomplete/jquery-ui.js"></script>


<script src="src/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="src/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>
    $(function () {
        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $("#tags").autocomplete({
            source: availableTags
        });
    });
</script>
<!-- Page script -->
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()

        //Date range picker
        $('#reservation').daterangepicker()
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'})
        //Date range as a button
        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        '15 days': [moment(), moment().add(15, 'days')],
                        'next month': [moment(), moment().add(1, 'month')],
                        '45 days': [moment(), moment().add(45, 'days')],
                        'next 2 month': [moment(), moment().add(2, 'month')],
                    },
                    startDate: moment(),
                    endDate: moment().add(1, 'month'),
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
        )

        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        })

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        })
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        })
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        })

        //Colorpicker
        $('.my-colorpicker1').colorpicker()
        //color picker with addon
        $('.my-colorpicker2').colorpicker()

        //Timepicker
        $('.timepicker').timepicker({
            showInputs: false
        })
        
        $("#created_at").datepicker().datepicker("setDate", new Date());
    })
</script>
<script src="src/dist/js/validator.min.js"></script>
<script src="src/bower_components/chart.js/Chart.js"></script>

<script type="text/javascript" src="src/dist/js/jquery.smartWizard.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

 $("#save_form").on('click', function () {
                    if (!$(this).hasClass('disabled')) {
                        var elmForm = $("#myForm");
                        if (elmForm) {
                            elmForm.validator('validate');
                            var elmErr = elmForm.find('.has-error');
                            if (elmErr && elmErr.length > 0) {
                                alert('please fill all required fields');
                                return false;
                            } else {
elmForm.attr('action','model/student/save.php');
                                elmForm.submit();
                                $(".finish").addClass('hide');
                                $("#save_form").addClass('hide');
                              
                                    Pace.restart()
                               
                                return false;
                            }
                        }
                    }
                });


        var btnFinish = $('<button></button>').text('Finish')
                .addClass('btn btn-primary finish hide')

                .on('click', function () {
                    if (!$(this).hasClass('disabled')) {
                        var elmForm = $("#myForm");
                        if (elmForm) {
                            elmForm.validator('validate');
                            var elmErr = elmForm.find('.has-error');
                            if (elmErr && elmErr.length > 0) {
                                alert('please fill all required fields');
                                return false;
                            } else {

                                elmForm.submit();
                                $(".finish").addClass('hide');
                                $("#save_form").addClass('hide');
                              
                                    Pace.restart()
                               
                                return false;
                            }
                        }
                    }
                });
        var btnCancel = $('<button></button>').text('Cancel')
                .addClass('btn btn-danger ')
                .on('click', function () {
                    $('#smartwizard').smartWizard("reset");
                    $('#myForm').find("input, textarea").val("");
                });

        // Step show event
        $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
            //alert("You are on step "+stepNumber+" now");
            if (stepPosition === 'first') {
                $("#prev-btn").addClass('disabled');

            } else if (stepPosition === 'final') {
                $("#next-btn").addClass('disabled');
                $('.finish').removeClass('hide');
                $("#save_form").removeClass('hide');
            } else {
                $("#prev-btn").removeClass('disabled');
                $("#next-btn").removeClass('disabled');

            }
        });

        $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
            var selectedyear = $(".year option:selected").val();

            if (selectedyear != 0) {
                
                var elmForm = $("#form-step-" + stepNumber);
                // stepDirection === 'forward' :- this condition allows to do the form validation
                // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
                if (stepDirection === 'forward' && elmForm) {
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if (elmErr && elmErr.length > 0) {
                        // Form validation failed
                        return false;
                    }
                }
                return true;

            } else {

                alert("Please Select Admission Year First")
                return false;
            }
        });


        // Smart Wizard 1
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'arrows',
            transitionEffect: 'fade',
            showStepURLhash: false,
            toolbarSettings: {toolbarPosition: 'bottom',
                toolbarExtraButtons: [btnFinish, btnCancel]

            }
        });



    });


    $(document).ajaxStart(function () {
        Pace.restart();
    })

    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });
    $(document).ready(function() {
        
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
    });
</script>

<!-- DataTables -->
<script src="src/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="src/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


</body>
</html>